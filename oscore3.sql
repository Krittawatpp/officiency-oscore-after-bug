/*
 Navicat Premium Data Transfer

 Source Server         : codecamp3
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : oscore3

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 19/06/2019 13:03:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for instructor_score
-- ----------------------------
DROP TABLE IF EXISTS `instructor_score`;
CREATE TABLE `instructor_score`  (
  `user_id` bigint(20) NULL DEFAULT NULL,
  `workshop_id` int(11) NULL DEFAULT NULL,
  `skill_id` int(20) NULL DEFAULT NULL,
  `skill_score` int(20) NULL DEFAULT NULL,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `workshop_id`(`workshop_id`) USING BTREE,
  INDEX `skill_id`(`skill_id`) USING BTREE,
  CONSTRAINT `instructor_score_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `instructor_score_ibfk_2` FOREIGN KEY (`workshop_id`) REFERENCES `workshop` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `instructor_score_ibfk_3` FOREIGN KEY (`skill_id`) REFERENCES `instructor_skill` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of instructor_score
-- ----------------------------
INSERT INTO `instructor_score` VALUES (3, 2, 1, 50);
INSERT INTO `instructor_score` VALUES (3, 2, 2, 99);
INSERT INTO `instructor_score` VALUES (3, 2, 3, 20);
INSERT INTO `instructor_score` VALUES (3, 2, 4, 90);
INSERT INTO `instructor_score` VALUES (3, 2, 5, 50);
INSERT INTO `instructor_score` VALUES (3, 2, 6, 50);
INSERT INTO `instructor_score` VALUES (3, 2, 1, 55);
INSERT INTO `instructor_score` VALUES (3, 2, 2, 77);
INSERT INTO `instructor_score` VALUES (3, 2, 3, 98);
INSERT INTO `instructor_score` VALUES (3, 2, 4, 18);
INSERT INTO `instructor_score` VALUES (3, 2, 5, 15);
INSERT INTO `instructor_score` VALUES (3, 2, 6, 19);
INSERT INTO `instructor_score` VALUES (3, 2, 1, 55);
INSERT INTO `instructor_score` VALUES (3, 2, 2, 55);
INSERT INTO `instructor_score` VALUES (3, 2, 3, 55);
INSERT INTO `instructor_score` VALUES (3, 2, 4, 55);
INSERT INTO `instructor_score` VALUES (3, 2, 5, 55);
INSERT INTO `instructor_score` VALUES (3, 2, 6, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 1, 66);
INSERT INTO `instructor_score` VALUES (3, 1, 2, 66);
INSERT INTO `instructor_score` VALUES (3, 1, 3, 66);
INSERT INTO `instructor_score` VALUES (3, 1, 4, 66);
INSERT INTO `instructor_score` VALUES (3, 1, 5, 66);
INSERT INTO `instructor_score` VALUES (3, 1, 6, 66);
INSERT INTO `instructor_score` VALUES (3, 1, 1, 45);
INSERT INTO `instructor_score` VALUES (3, 1, 2, 45);
INSERT INTO `instructor_score` VALUES (3, 1, 3, 45);
INSERT INTO `instructor_score` VALUES (3, 1, 4, 45);
INSERT INTO `instructor_score` VALUES (3, 1, 5, 45);
INSERT INTO `instructor_score` VALUES (3, 1, 6, 45);
INSERT INTO `instructor_score` VALUES (3, 1, 1, 77);
INSERT INTO `instructor_score` VALUES (3, 1, 2, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 3, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 4, 77);
INSERT INTO `instructor_score` VALUES (3, 1, 5, 77);
INSERT INTO `instructor_score` VALUES (3, 1, 6, 77);
INSERT INTO `instructor_score` VALUES (3, 2, 1, 22);
INSERT INTO `instructor_score` VALUES (3, 2, 2, 22);
INSERT INTO `instructor_score` VALUES (3, 2, 3, 22);
INSERT INTO `instructor_score` VALUES (3, 2, 4, 22);
INSERT INTO `instructor_score` VALUES (3, 2, 5, 22);
INSERT INTO `instructor_score` VALUES (3, 2, 6, 22);
INSERT INTO `instructor_score` VALUES (3, 1, 1, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 2, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 3, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 4, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 5, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 6, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 1, 22);
INSERT INTO `instructor_score` VALUES (3, 1, 2, 222);
INSERT INTO `instructor_score` VALUES (3, 1, 3, 22);
INSERT INTO `instructor_score` VALUES (3, 1, 4, 22);
INSERT INTO `instructor_score` VALUES (3, 1, 5, 22);
INSERT INTO `instructor_score` VALUES (3, 1, 6, 22);
INSERT INTO `instructor_score` VALUES (3, 1, 1, 33);
INSERT INTO `instructor_score` VALUES (3, 1, 2, 33);
INSERT INTO `instructor_score` VALUES (3, 1, 3, 33);
INSERT INTO `instructor_score` VALUES (3, 1, 4, 33);
INSERT INTO `instructor_score` VALUES (3, 1, 5, 33);
INSERT INTO `instructor_score` VALUES (3, 1, 6, 33);
INSERT INTO `instructor_score` VALUES (3, 1, 1, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 2, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 3, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 4, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 5, 55);
INSERT INTO `instructor_score` VALUES (3, 1, 6, 55);

-- ----------------------------
-- Table structure for instructor_skill
-- ----------------------------
DROP TABLE IF EXISTS `instructor_skill`;
CREATE TABLE `instructor_skill`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`, `skill_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of instructor_skill
-- ----------------------------
INSERT INTO `instructor_skill` VALUES (1, 'Presentation');
INSERT INTO `instructor_skill` VALUES (2, 'Skills');
INSERT INTO `instructor_skill` VALUES (3, 'Communication');
INSERT INTO `instructor_skill` VALUES (4, 'Time controll');
INSERT INTO `instructor_skill` VALUES (5, 'Knowledge');
INSERT INTO `instructor_skill` VALUES (6, 'Others');

-- ----------------------------
-- Table structure for persistent_logins
-- ----------------------------
DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins`  (
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `last_used` timestamp(0) NOT NULL,
  PRIMARY KEY (`series`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `role_uk`(`role_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'admin');
INSERT INTO `role` VALUES (3, 'instructor');
INSERT INTO `role` VALUES (4, 'student');
INSERT INTO `role` VALUES (2, 'user');

-- ----------------------------
-- Table structure for skill
-- ----------------------------
DROP TABLE IF EXISTS `skill`;
CREATE TABLE `skill`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of skill
-- ----------------------------
INSERT INTO `skill` VALUES (1, 'S1');
INSERT INTO `skill` VALUES (2, 'S2');
INSERT INTO `skill` VALUES (3, 'S3');
INSERT INTO `skill` VALUES (4, 'S4');
INSERT INTO `skill` VALUES (5, 'S5');
INSERT INTO `skill` VALUES (6, 'S6');
INSERT INTO `skill` VALUES (7, 'S7');
INSERT INTO `skill` VALUES (8, 'S8');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `tel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `user_uk`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'a', 'a', 'a', 'a', NULL, NULL, b'1');
INSERT INTO `user` VALUES (2, 'b', 'b', 'b', 'b', NULL, NULL, b'1');
INSERT INTO `user` VALUES (3, 'c', 'c', 'c', 'c', NULL, NULL, b'1');
INSERT INTO `user` VALUES (4, 'd', 'd', 'd', 'd', NULL, NULL, b'1');
INSERT INTO `user` VALUES (5, 'e', 'e', 'e', 'e', NULL, NULL, b'1');
INSERT INTO `user` VALUES (6, 'f', 'f', 'f', 'f', NULL, NULL, b'1');
INSERT INTO `user` VALUES (7, 'g', 'g', 'g', 'g', NULL, NULL, b'1');
INSERT INTO `user` VALUES (8, 'h', 'h', 'h', 'h', NULL, NULL, b'1');
INSERT INTO `user` VALUES (9, 'i', 'i', 'i', 'i', NULL, NULL, b'1');
INSERT INTO `user` VALUES (10, 'j', 'j', 'j', 'j', NULL, NULL, b'1');
INSERT INTO `user` VALUES (11, 'k', 'k', 'k', 'k', NULL, NULL, b'1');
INSERT INTO `user` VALUES (12, 'l', 'l', 'l', 'l', NULL, NULL, b'1');
INSERT INTO `user` VALUES (13, 'krittawat.pp@gmail.com', '$2a$10$uDzmCiK9KIWpioDkBLWB4eGQpoQvzfdVVv8FMMfxvOgRGIjiPSQsO', NULL, NULL, NULL, NULL, b'1');
INSERT INTO `user` VALUES (14, 'krittawat.pp2@gmail.com', '$2a$10$JNoXD850eBvEtCGoHSHjPuylNQHS3tCFt1MG//Bx9pqYN3ZRBkuTO', NULL, NULL, NULL, NULL, b'1');
INSERT INTO `user` VALUES (15, 'krittawat.pp3@gmail.com', '$2a$10$EVcxazzsabCjdFZdAZBa7.vaAJNxcYwIh3qjpkgf9odFttWZ2gCGi', NULL, NULL, NULL, NULL, b'1');
INSERT INTO `user` VALUES (16, 'krittawat.pp4@gmail.com', '$2a$10$1ymUD8Iqq/cvLFtWw1HS3.pHU56qvGl8aUhkPA0O7C3iSNgzEJCq.', 'Krittawat', 'Wisedsuwon', NULL, '0968215262', b'1');
INSERT INTO `user` VALUES (17, '', '$2a$10$KicI9HJwE/FgFPeoRiFkduI8f/uk/MPpe38ManBHXJvZaS1OaYu1u', '', '', '', '', b'1');
INSERT INTO `user` VALUES (18, 'krittawat.pp6@gmai.com', '$2a$10$UC2wCJvdlds8bVcaA.KBcuaPq90At9oKOMiT28Prxh9PW6qhGL/VO', 'Krittawat', 'Wisedsuwon', NULL, '096821526', b'1');
INSERT INTO `user` VALUES (19, 'rtfff@gmail.com', '$2a$10$BGzOKputkhLt7kJem.6.3e.uhj3S.S5F7cpkys4DS5VYjRaUTgNai', NULL, NULL, NULL, NULL, b'1');
INSERT INTO `user` VALUES (20, 'krittawat.pp6@gmail.com', '$2a$10$1fuGRgk.HjzsujyYSrkiyOwAIPqN67oqP1GYB/nm0Pog6OfDj8dGq', NULL, NULL, NULL, NULL, b'1');
INSERT INTO `user` VALUES (21, 'krittawat.pp67@gmail.com', '$2a$10$DKWsd8FQ3A0e6.6T9TUZKupefvWPIWMsKlII6uI5yOQF8PdyUrIMq', NULL, NULL, NULL, NULL, b'1');
INSERT INTO `user` VALUES (22, 'jaid@co.th', 'a1234', 'asasfasf', 'asdas', 'asasf', '561561', b'1');
INSERT INTO `user` VALUES (23, 'prach@gmail.com', '$2a$10$1d9b8zIhKbVn0OmCCMfI5OP.NBNkWShPK9hhnPFy4kce2yaez.9Te', NULL, NULL, NULL, NULL, b'1');
INSERT INTO `user` VALUES (24, 'test@gmail.com', '$2a$10$O23bHZnCumML8yTgBbDHr.q7kwos8W3h4LZx4LhkCPaSDqdaUvoL2', NULL, NULL, NULL, NULL, b'1');
INSERT INTO `user` VALUES (25, 'test2@gmail.com', '$2a$10$iSv5h0R1TIAUsQUb0gdq8uYx9LGrMGQoL9qfBjn85qfSLjWz1FnTK', NULL, NULL, NULL, NULL, b'1');
INSERT INTO `user` VALUES (27, 'test3@gmail.com', '$2a$10$XsXAEopcsnlqFkgn0t1Qz.1vsncgv6xXiVIY9ZsmdBg1W6pt5T6zW', 'Krittawat', 'Wisedsuwon', 'Prach', '968215262', b'1');
INSERT INTO `user` VALUES (28, 'test4@gmail.com', '$2a$10$DXbmi153Iu0r8okQowFbkexLMdwjRLf/ah8OFoOhMTK/jTDf9S1nG', NULL, NULL, NULL, NULL, b'1');

-- ----------------------------
-- Table structure for user_att_workshop
-- ----------------------------
DROP TABLE IF EXISTS `user_att_workshop`;
CREATE TABLE `user_att_workshop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `workshop_att_id` int(11) NOT NULL,
  `group_number` int(11) NOT NULL,
  `skill_id` int(11) NULL DEFAULT NULL,
  `skill_score` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `workshop_att_id`(`workshop_att_id`) USING BTREE,
  INDEX `group_number`(`group_number`) USING BTREE,
  INDEX `skill_id`(`skill_id`) USING BTREE,
  CONSTRAINT `user_att_workshop_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_att_workshop_ibfk_2` FOREIGN KEY (`workshop_att_id`) REFERENCES `workshop` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_att_workshop_ibfk_3` FOREIGN KEY (`group_number`) REFERENCES `workshop_group` (`group_number`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_att_workshop_ibfk_4` FOREIGN KEY (`skill_id`) REFERENCES `workshop_skill` (`skill_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 137 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_att_workshop
-- ----------------------------
INSERT INTO `user_att_workshop` VALUES (3, 5, 1, 1, 1, '50');
INSERT INTO `user_att_workshop` VALUES (4, 5, 1, 1, 1, '42');
INSERT INTO `user_att_workshop` VALUES (5, 5, 1, 1, 1, '30');
INSERT INTO `user_att_workshop` VALUES (6, 5, 1, 1, 1, '22');
INSERT INTO `user_att_workshop` VALUES (7, 5, 1, 1, 2, '18');
INSERT INTO `user_att_workshop` VALUES (8, 5, 1, 1, 2, '62');
INSERT INTO `user_att_workshop` VALUES (9, 5, 1, 1, 2, '57');
INSERT INTO `user_att_workshop` VALUES (10, 5, 1, 1, 2, '95');
INSERT INTO `user_att_workshop` VALUES (11, 5, 1, 1, 3, '150');
INSERT INTO `user_att_workshop` VALUES (12, 5, 1, 1, 3, '187');
INSERT INTO `user_att_workshop` VALUES (13, 5, 1, 1, 3, '193');
INSERT INTO `user_att_workshop` VALUES (14, 5, 1, 1, 3, '200');
INSERT INTO `user_att_workshop` VALUES (15, 5, 1, 1, 4, '50');
INSERT INTO `user_att_workshop` VALUES (16, 5, 1, 1, 4, '36');
INSERT INTO `user_att_workshop` VALUES (17, 5, 1, 1, 4, '180');
INSERT INTO `user_att_workshop` VALUES (18, 5, 1, 1, 4, '50');
INSERT INTO `user_att_workshop` VALUES (19, 6, 1, 2, 1, '50');
INSERT INTO `user_att_workshop` VALUES (20, 6, 1, 2, 1, '42');
INSERT INTO `user_att_workshop` VALUES (21, 6, 1, 2, 1, '33');
INSERT INTO `user_att_workshop` VALUES (22, 6, 1, 2, 1, '22');
INSERT INTO `user_att_workshop` VALUES (23, 6, 1, 2, 2, '18');
INSERT INTO `user_att_workshop` VALUES (24, 6, 1, 2, 2, '62');
INSERT INTO `user_att_workshop` VALUES (25, 6, 1, 2, 2, '57');
INSERT INTO `user_att_workshop` VALUES (26, 6, 1, 2, 2, '95');
INSERT INTO `user_att_workshop` VALUES (27, 6, 1, 2, 3, '150');
INSERT INTO `user_att_workshop` VALUES (28, 6, 1, 2, 3, '187');
INSERT INTO `user_att_workshop` VALUES (29, 6, 1, 2, 3, '193');
INSERT INTO `user_att_workshop` VALUES (30, 6, 1, 2, 3, '200');
INSERT INTO `user_att_workshop` VALUES (31, 6, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (32, 6, 1, 2, 4, '36');
INSERT INTO `user_att_workshop` VALUES (33, 6, 1, 2, 4, '180');
INSERT INTO `user_att_workshop` VALUES (34, 6, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (35, 7, 1, 2, 1, '50');
INSERT INTO `user_att_workshop` VALUES (36, 7, 1, 2, 1, '42');
INSERT INTO `user_att_workshop` VALUES (37, 7, 1, 2, 1, '33');
INSERT INTO `user_att_workshop` VALUES (38, 7, 1, 2, 1, '22');
INSERT INTO `user_att_workshop` VALUES (39, 7, 1, 2, 2, '18');
INSERT INTO `user_att_workshop` VALUES (40, 7, 1, 2, 2, '62');
INSERT INTO `user_att_workshop` VALUES (41, 7, 1, 2, 2, '57');
INSERT INTO `user_att_workshop` VALUES (42, 7, 1, 2, 2, '95');
INSERT INTO `user_att_workshop` VALUES (43, 7, 1, 2, 3, '150');
INSERT INTO `user_att_workshop` VALUES (44, 7, 1, 2, 3, '187');
INSERT INTO `user_att_workshop` VALUES (45, 7, 1, 2, 3, '193');
INSERT INTO `user_att_workshop` VALUES (46, 7, 1, 2, 3, '200');
INSERT INTO `user_att_workshop` VALUES (47, 7, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (48, 7, 1, 2, 4, '36');
INSERT INTO `user_att_workshop` VALUES (49, 7, 1, 2, 4, '180');
INSERT INTO `user_att_workshop` VALUES (50, 7, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (51, 8, 1, 2, 1, '50');
INSERT INTO `user_att_workshop` VALUES (52, 8, 1, 2, 1, '42');
INSERT INTO `user_att_workshop` VALUES (53, 8, 1, 2, 1, '33');
INSERT INTO `user_att_workshop` VALUES (54, 8, 1, 2, 1, '22');
INSERT INTO `user_att_workshop` VALUES (55, 8, 1, 2, 2, '18');
INSERT INTO `user_att_workshop` VALUES (56, 8, 1, 2, 2, '62');
INSERT INTO `user_att_workshop` VALUES (57, 8, 1, 2, 2, '57');
INSERT INTO `user_att_workshop` VALUES (58, 8, 1, 2, 2, '95');
INSERT INTO `user_att_workshop` VALUES (59, 8, 1, 2, 3, '150');
INSERT INTO `user_att_workshop` VALUES (60, 8, 1, 2, 3, '187');
INSERT INTO `user_att_workshop` VALUES (61, 8, 1, 2, 3, '193');
INSERT INTO `user_att_workshop` VALUES (62, 8, 1, 2, 3, '200');
INSERT INTO `user_att_workshop` VALUES (63, 8, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (64, 8, 1, 2, 4, '36');
INSERT INTO `user_att_workshop` VALUES (65, 8, 1, 2, 4, '180');
INSERT INTO `user_att_workshop` VALUES (66, 8, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (67, 9, 1, 2, 1, '50');
INSERT INTO `user_att_workshop` VALUES (68, 9, 1, 2, 1, '42');
INSERT INTO `user_att_workshop` VALUES (69, 9, 1, 2, 1, '33');
INSERT INTO `user_att_workshop` VALUES (70, 9, 1, 2, 1, '22');
INSERT INTO `user_att_workshop` VALUES (71, 9, 1, 2, 2, '18');
INSERT INTO `user_att_workshop` VALUES (72, 9, 1, 2, 2, '62');
INSERT INTO `user_att_workshop` VALUES (73, 9, 1, 2, 2, '57');
INSERT INTO `user_att_workshop` VALUES (74, 9, 1, 2, 2, '95');
INSERT INTO `user_att_workshop` VALUES (75, 9, 1, 2, 3, '150');
INSERT INTO `user_att_workshop` VALUES (76, 9, 1, 2, 3, '187');
INSERT INTO `user_att_workshop` VALUES (77, 9, 1, 2, 3, '193');
INSERT INTO `user_att_workshop` VALUES (78, 9, 1, 2, 3, '200');
INSERT INTO `user_att_workshop` VALUES (79, 9, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (80, 9, 1, 2, 4, '36');
INSERT INTO `user_att_workshop` VALUES (81, 9, 1, 2, 4, '180');
INSERT INTO `user_att_workshop` VALUES (82, 9, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (83, 5, 2, 4, 5, '55');
INSERT INTO `user_att_workshop` VALUES (84, 5, 2, 4, 6, '0');
INSERT INTO `user_att_workshop` VALUES (85, 5, 2, 4, 7, '200');
INSERT INTO `user_att_workshop` VALUES (86, 5, 2, 4, 8, '95');
INSERT INTO `user_att_workshop` VALUES (87, 5, 2, 4, 5, '77');
INSERT INTO `user_att_workshop` VALUES (88, 5, 2, 4, 6, '53');
INSERT INTO `user_att_workshop` VALUES (89, 5, 2, 4, 7, '48');
INSERT INTO `user_att_workshop` VALUES (90, 5, 2, 4, 8, '32');
INSERT INTO `user_att_workshop` VALUES (91, 5, 2, 4, 5, '14');
INSERT INTO `user_att_workshop` VALUES (92, 5, 2, 4, 6, '20');
INSERT INTO `user_att_workshop` VALUES (93, 5, 2, 4, 7, '33');
INSERT INTO `user_att_workshop` VALUES (94, 5, 2, 4, 8, '69');
INSERT INTO `user_att_workshop` VALUES (95, 5, 2, 4, 5, '57');
INSERT INTO `user_att_workshop` VALUES (96, 5, 2, 4, 6, '22');
INSERT INTO `user_att_workshop` VALUES (97, 5, 2, 4, 7, '32');
INSERT INTO `user_att_workshop` VALUES (98, 5, 2, 4, 8, '170');
INSERT INTO `user_att_workshop` VALUES (101, 9, 4, 1, 1, '166');
INSERT INTO `user_att_workshop` VALUES (102, 17, 1, 2, 1, '50');
INSERT INTO `user_att_workshop` VALUES (103, 17, 1, 2, 1, '42');
INSERT INTO `user_att_workshop` VALUES (104, 17, 1, 2, 1, '33');
INSERT INTO `user_att_workshop` VALUES (105, 17, 1, 2, 1, '22');
INSERT INTO `user_att_workshop` VALUES (106, 17, 1, 2, 2, '18');
INSERT INTO `user_att_workshop` VALUES (107, 17, 1, 2, 2, '62');
INSERT INTO `user_att_workshop` VALUES (108, 17, 1, 2, 2, '57');
INSERT INTO `user_att_workshop` VALUES (109, 17, 1, 2, 2, '95');
INSERT INTO `user_att_workshop` VALUES (110, 17, 1, 2, 3, '150');
INSERT INTO `user_att_workshop` VALUES (111, 17, 1, 2, 3, '187');
INSERT INTO `user_att_workshop` VALUES (112, 17, 1, 2, 3, '193');
INSERT INTO `user_att_workshop` VALUES (113, 17, 1, 2, 3, '200');
INSERT INTO `user_att_workshop` VALUES (114, 17, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (115, 17, 1, 2, 4, '36');
INSERT INTO `user_att_workshop` VALUES (116, 17, 1, 2, 4, '180');
INSERT INTO `user_att_workshop` VALUES (117, 17, 1, 2, 4, '50');
INSERT INTO `user_att_workshop` VALUES (118, 17, 1, 2, 1, '1502');
INSERT INTO `user_att_workshop` VALUES (119, 17, 1, 2, 2, '5555');
INSERT INTO `user_att_workshop` VALUES (120, 17, 1, 2, 3, '5555');
INSERT INTO `user_att_workshop` VALUES (121, 17, 1, 2, 4, '5555');
INSERT INTO `user_att_workshop` VALUES (122, 5, 2, 2, 5, '33');
INSERT INTO `user_att_workshop` VALUES (123, 5, 2, 2, 6, '33');
INSERT INTO `user_att_workshop` VALUES (124, 5, 2, 2, 7, '33');
INSERT INTO `user_att_workshop` VALUES (125, 5, 2, 2, 8, '33');
INSERT INTO `user_att_workshop` VALUES (126, 6, 1, 2, 1, '22');
INSERT INTO `user_att_workshop` VALUES (127, 6, 1, 2, 2, '82');
INSERT INTO `user_att_workshop` VALUES (128, 6, 1, 2, 3, '22');
INSERT INTO `user_att_workshop` VALUES (129, 6, 1, 2, 4, '22');
INSERT INTO `user_att_workshop` VALUES (130, 24, 1, 2, 1, '33');
INSERT INTO `user_att_workshop` VALUES (131, 24, 1, 2, 2, '33');
INSERT INTO `user_att_workshop` VALUES (132, 24, 1, 2, 3, '33');
INSERT INTO `user_att_workshop` VALUES (133, 24, 1, 2, 4, '33');
INSERT INTO `user_att_workshop` VALUES (134, 27, 1, 2, 1, '55');
INSERT INTO `user_att_workshop` VALUES (135, 27, 1, 2, 2, '55');
INSERT INTO `user_att_workshop` VALUES (136, 27, 1, 2, 3, '55');
INSERT INTO `user_att_workshop` VALUES (137, 27, 1, 2, 4, '55');

-- ----------------------------
-- Table structure for user_feedback
-- ----------------------------
DROP TABLE IF EXISTS `user_feedback`;
CREATE TABLE `user_feedback`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `workshop_attend_id` int(11) NULL DEFAULT NULL,
  `group_number` int(11) NULL DEFAULT NULL,
  `feedback` varchar(1200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `suggestion` varchar(1200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `workshop_attend_id`(`workshop_attend_id`) USING BTREE,
  INDEX `group_number`(`group_number`) USING BTREE,
  CONSTRAINT `user_feedback_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_feedback_ibfk_2` FOREIGN KEY (`workshop_attend_id`) REFERENCES `workshop` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_feedback_ibfk_3` FOREIGN KEY (`group_number`) REFERENCES `workshop_group` (`group_number`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_feedback
-- ----------------------------
INSERT INTO `user_feedback` VALUES (1, 5, 1, 1, 'Good boy', 'Sleep more');
INSERT INTO `user_feedback` VALUES (2, 5, 1, 1, 'Good boy 2', 'Sleep more 2');
INSERT INTO `user_feedback` VALUES (3, 5, 1, 1, 'Good boy 3', 'Sleep more 3');
INSERT INTO `user_feedback` VALUES (4, 5, 1, 1, '\"Good boy 4\"', '\"Sleep more 4\"');
INSERT INTO `user_feedback` VALUES (5, 17, 1, 2, 'Good boy', 'Sleep more');
INSERT INTO `user_feedback` VALUES (6, 17, 1, 2, 'Good boy 2', 'Sleep more 2');
INSERT INTO `user_feedback` VALUES (7, 17, 1, 2, 'Good boy 3', 'Sleep more 3');
INSERT INTO `user_feedback` VALUES (8, 17, 1, 2, 'Good boy 4', 'Sleep more 4');
INSERT INTO `user_feedback` VALUES (9, 3, 2, 2, 'asfsafa', '');
INSERT INTO `user_feedback` VALUES (10, 3, 2, 2, '151', '');
INSERT INTO `user_feedback` VALUES (11, 3, 1, 2, 'afs', '');
INSERT INTO `user_feedback` VALUES (12, 3, 1, 2, 'sdfs', 'awqs');
INSERT INTO `user_feedback` VALUES (13, 17, 1, 2, 'ASD', 'ASDA');
INSERT INTO `user_feedback` VALUES (14, 3, 1, 2, 'adasf', 'afssasfa');
INSERT INTO `user_feedback` VALUES (15, 3, 2, 2, 'rtyuiuhg', 'ytfghjk');
INSERT INTO `user_feedback` VALUES (16, 5, 2, 2, 'rfghikjn', 'ygfhjj');
INSERT INTO `user_feedback` VALUES (17, 3, 1, 2, 'pkpk', 'pk[k');
INSERT INTO `user_feedback` VALUES (18, 3, 1, 2, 'dfsdf', 'awff');
INSERT INTO `user_feedback` VALUES (19, 6, 1, 2, 'sddf', 'fqwfqw');
INSERT INTO `user_feedback` VALUES (20, 3, 1, 2, 'swfwfa', 'asfasf');
INSERT INTO `user_feedback` VALUES (21, 24, 1, 2, 'asff', 'sdssfd');
INSERT INTO `user_feedback` VALUES (22, 3, 1, 2, 'aDAd', 'asasf');
INSERT INTO `user_feedback` VALUES (23, 27, 1, 2, 'adaFC', 'ASFASF');

-- ----------------------------
-- Table structure for user_group
-- ----------------------------
DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `workshop_id` int(11) NULL DEFAULT NULL,
  `group_number` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `workshop_id`(`workshop_id`) USING BTREE,
  INDEX `group_number`(`group_number`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `user_group_ibfk_1` FOREIGN KEY (`workshop_id`) REFERENCES `workshop_group` (`workshop_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_group_ibfk_2` FOREIGN KEY (`group_number`) REFERENCES `workshop_group` (`group_number`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_group_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_group
-- ----------------------------
INSERT INTO `user_group` VALUES (1, 5, 1, 1);
INSERT INTO `user_group` VALUES (2, 6, 1, 2);
INSERT INTO `user_group` VALUES (3, 7, 1, 2);
INSERT INTO `user_group` VALUES (4, 8, 1, 2);
INSERT INTO `user_group` VALUES (5, 9, 1, 2);
INSERT INTO `user_group` VALUES (6, 5, 4, 1);
INSERT INTO `user_group` VALUES (7, 8, 2, 1);
INSERT INTO `user_group` VALUES (8, 7, 2, 1);
INSERT INTO `user_group` VALUES (9, 9, 2, 1);
INSERT INTO `user_group` VALUES (10, 5, 2, 2);
INSERT INTO `user_group` VALUES (11, 6, 2, 2);
INSERT INTO `user_group` VALUES (39, 17, 1, 2);
INSERT INTO `user_group` VALUES (40, 17, 1, 3);
INSERT INTO `user_group` VALUES (41, 17, 1, 2);
INSERT INTO `user_group` VALUES (42, 17, 1, 2);
INSERT INTO `user_group` VALUES (56, 17, 2, 2);
INSERT INTO `user_group` VALUES (59, 23, 2, 2);
INSERT INTO `user_group` VALUES (60, 17, 2, 2);
INSERT INTO `user_group` VALUES (61, 17, 2, 2);
INSERT INTO `user_group` VALUES (62, 17, 2, 2);
INSERT INTO `user_group` VALUES (63, 17, 2, 2);
INSERT INTO `user_group` VALUES (64, 17, 1, 2);
INSERT INTO `user_group` VALUES (65, 17, 1, 2);
INSERT INTO `user_group` VALUES (66, 17, 1, 2);
INSERT INTO `user_group` VALUES (67, 17, 1, 2);
INSERT INTO `user_group` VALUES (68, 17, 1, 2);
INSERT INTO `user_group` VALUES (69, 22, 2, 2);
INSERT INTO `user_group` VALUES (70, 17, 1, 2);
INSERT INTO `user_group` VALUES (71, 24, 1, 2);
INSERT INTO `user_group` VALUES (72, 24, 1, 2);
INSERT INTO `user_group` VALUES (73, 27, 1, 2);

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_role_uk`(`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (1, 1, 1);
INSERT INTO `user_role` VALUES (2, 2, 2);
INSERT INTO `user_role` VALUES (3, 3, 3);
INSERT INTO `user_role` VALUES (4, 4, 3);
INSERT INTO `user_role` VALUES (5, 5, 4);
INSERT INTO `user_role` VALUES (6, 6, 4);
INSERT INTO `user_role` VALUES (7, 7, 4);
INSERT INTO `user_role` VALUES (8, 8, 4);
INSERT INTO `user_role` VALUES (9, 9, 4);
INSERT INTO `user_role` VALUES (10, 10, 4);
INSERT INTO `user_role` VALUES (11, 11, 4);
INSERT INTO `user_role` VALUES (12, 12, 4);
INSERT INTO `user_role` VALUES (13, 13, 3);
INSERT INTO `user_role` VALUES (14, 14, 4);
INSERT INTO `user_role` VALUES (15, 15, 4);
INSERT INTO `user_role` VALUES (16, 16, 3);
INSERT INTO `user_role` VALUES (17, 17, 3);
INSERT INTO `user_role` VALUES (18, 18, 3);
INSERT INTO `user_role` VALUES (19, 19, 4);
INSERT INTO `user_role` VALUES (20, 20, 4);
INSERT INTO `user_role` VALUES (21, 21, 4);
INSERT INTO `user_role` VALUES (22, 22, 4);
INSERT INTO `user_role` VALUES (23, 23, 4);
INSERT INTO `user_role` VALUES (24, 24, 4);
INSERT INTO `user_role` VALUES (25, 25, 4);
INSERT INTO `user_role` VALUES (26, 27, 4);
INSERT INTO `user_role` VALUES (27, 28, 4);

-- ----------------------------
-- Table structure for workshop
-- ----------------------------
DROP TABLE IF EXISTS `workshop`;
CREATE TABLE `workshop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_by_id` bigint(20) NOT NULL,
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(1200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `start` date NOT NULL,
  `end` date NULL DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE,
  INDEX `create_by_id`(`create_by_id`) USING BTREE,
  CONSTRAINT `workshop_ibfk_1` FOREIGN KEY (`create_by_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of workshop
-- ----------------------------
INSERT INTO `workshop` VALUES (1, 3, 'CC3', 'java', '2019-05-29', NULL, b'1');
INSERT INTO `workshop` VALUES (2, 3, 'CC2', 'java', '2019-05-29', NULL, b'1');
INSERT INTO `workshop` VALUES (3, 4, 'CK1', 'code', '2019-05-29', NULL, b'1');
INSERT INTO `workshop` VALUES (4, 4, 'CK2', 'code', '2019-05-29', NULL, b'1');
INSERT INTO `workshop` VALUES (5, 4, 'jaja', 'jj', '2019-05-29', NULL, b'0');
INSERT INTO `workshop` VALUES (6, 4, 'dd', 'dd', '2019-05-29', NULL, b'0');
INSERT INTO `workshop` VALUES (7, 4, 'kk', 'kk', '2019-05-29', NULL, b'0');
INSERT INTO `workshop` VALUES (8, 3, 'idk', NULL, '2019-03-16', NULL, b'0');
INSERT INTO `workshop` VALUES (9, 4, '\"eeee\"', NULL, '2019-06-12', NULL, b'0');
INSERT INTO `workshop` VALUES (10, 4, 'testenabled', NULL, '2019-05-31', NULL, b'0');
INSERT INTO `workshop` VALUES (12, 17, 'testenabled2', NULL, '2019-05-31', NULL, b'0');
INSERT INTO `workshop` VALUES (13, 17, 'testenabled3', 'sfsfsfs', '2019-05-31', '2019-06-10', b'1');
INSERT INTO `workshop` VALUES (14, 17, 'testenabled4', NULL, '2019-05-31', NULL, b'1');
INSERT INTO `workshop` VALUES (15, 17, 'testenabled5', NULL, '2019-05-31', NULL, b'1');
INSERT INTO `workshop` VALUES (16, 17, 'testenabled6', NULL, '2019-05-31', NULL, b'1');
INSERT INTO `workshop` VALUES (17, 17, 'Testenabled7', NULL, '2019-05-29', NULL, b'1');
INSERT INTO `workshop` VALUES (18, 17, 'KKKKKK', NULL, '2019-05-31', NULL, b'1');
INSERT INTO `workshop` VALUES (19, 17, 'kakakakak', NULL, '2019-05-31', NULL, b'1');
INSERT INTO `workshop` VALUES (20, 17, 'Hello world', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (21, 17, 'Hello world2', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (22, 17, 'Hello world3', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (23, 17, 'Hello world 4', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (24, 17, 'Hello world 5', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (25, 17, 'Hello world 6', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (26, 17, 'Hello world 8', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (27, 17, 'Hello world 9', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (28, 17, 'Hello world 10', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (29, 17, 'Tonsai', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (30, 17, 'Hello world 111', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (31, 17, 'Hello world 115', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (32, 17, 'P Max 555', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (33, 17, 'Hello world as', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (34, 17, 'Hello world sfawf', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (35, 17, 'effqwfwq', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (36, 17, 'safsafasf', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (37, 17, 'safsfsafasf', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (38, 17, 'asffasf', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (39, 17, 'asfsafasfasf', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (40, 17, 'efsdfsf', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (41, 17, 'xzxvasvass', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (44, 17, 'sdafsagdsg', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (45, 17, 'drdfh', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (46, 17, 'Hello ff', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (47, 17, 'Hello worldfc', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (48, 17, 'Hello worldff', NULL, '2019-06-13', NULL, b'1');
INSERT INTO `workshop` VALUES (49, 17, 'dasss', NULL, '2019-06-21', NULL, b'1');
INSERT INTO `workshop` VALUES (50, 17, 'dddd', NULL, '2019-06-12', NULL, b'1');
INSERT INTO `workshop` VALUES (51, 17, 'iii', NULL, '2019-06-14', NULL, b'1');
INSERT INTO `workshop` VALUES (52, 17, 'Hello worldsss', NULL, '2019-06-08', NULL, b'1');
INSERT INTO `workshop` VALUES (53, 17, 'ddddaa', 'asssadas', '2019-06-15', '2019-06-12', b'1');
INSERT INTO `workshop` VALUES (54, 17, 'asfggg', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (55, 17, 'svb', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (56, 17, '53952', NULL, '2019-06-21', NULL, b'1');
INSERT INTO `workshop` VALUES (57, 17, 'sf11', NULL, '2019-06-22', NULL, b'1');
INSERT INTO `workshop` VALUES (58, 17, 'testtest', NULL, '2019-06-01', NULL, b'1');
INSERT INTO `workshop` VALUES (59, 17, 'Hello world 55 ', NULL, '2019-06-02', NULL, b'1');
INSERT INTO `workshop` VALUES (60, 17, 'Hello world fak', NULL, '2019-06-11', NULL, b'1');
INSERT INTO `workshop` VALUES (61, 17, 'Hello world 888', NULL, '2019-06-14', NULL, b'1');
INSERT INTO `workshop` VALUES (62, 17, 'Hello world 88888', NULL, '2019-06-20', NULL, b'1');
INSERT INTO `workshop` VALUES (63, 17, 'dsfsdfsd', NULL, '2019-06-06', NULL, b'1');
INSERT INTO `workshop` VALUES (64, 17, 'Hello world jj', NULL, '2019-06-06', NULL, b'1');
INSERT INTO `workshop` VALUES (65, 17, 'asfsasf', NULL, '2019-06-09', NULL, b'1');
INSERT INTO `workshop` VALUES (66, 17, 'Gotti', NULL, '2019-06-11', NULL, b'1');
INSERT INTO `workshop` VALUES (67, 24, 'test', NULL, '2019-06-11', NULL, b'1');
INSERT INTO `workshop` VALUES (68, 27, 'Tester', 'Test after deploy', '2019-06-11', '2019-06-11', b'1');

-- ----------------------------
-- Table structure for workshop_group
-- ----------------------------
DROP TABLE IF EXISTS `workshop_group`;
CREATE TABLE `workshop_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workshop_id` int(11) NULL DEFAULT NULL,
  `group_number` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `workshop_id`(`workshop_id`) USING BTREE,
  INDEX `group_number`(`group_number`) USING BTREE,
  CONSTRAINT `workshop_group_ibfk_1` FOREIGN KEY (`workshop_id`) REFERENCES `workshop` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 130 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of workshop_group
-- ----------------------------
INSERT INTO `workshop_group` VALUES (1, 1, 1);
INSERT INTO `workshop_group` VALUES (2, 1, 2);
INSERT INTO `workshop_group` VALUES (3, 1, 3);
INSERT INTO `workshop_group` VALUES (4, 1, 4);
INSERT INTO `workshop_group` VALUES (5, 2, 1);
INSERT INTO `workshop_group` VALUES (6, 2, 2);
INSERT INTO `workshop_group` VALUES (7, 2, 3);
INSERT INTO `workshop_group` VALUES (8, 2, 4);
INSERT INTO `workshop_group` VALUES (9, 3, 1);
INSERT INTO `workshop_group` VALUES (10, 3, 2);
INSERT INTO `workshop_group` VALUES (11, 3, 3);
INSERT INTO `workshop_group` VALUES (12, 3, 4);
INSERT INTO `workshop_group` VALUES (13, 4, 1);
INSERT INTO `workshop_group` VALUES (14, 4, 2);
INSERT INTO `workshop_group` VALUES (15, 4, 3);
INSERT INTO `workshop_group` VALUES (16, 4, 4);
INSERT INTO `workshop_group` VALUES (17, 9, 1);
INSERT INTO `workshop_group` VALUES (18, 9, 2);
INSERT INTO `workshop_group` VALUES (19, 9, 3);
INSERT INTO `workshop_group` VALUES (20, 9, 4);
INSERT INTO `workshop_group` VALUES (21, 18, 1);
INSERT INTO `workshop_group` VALUES (22, 16, 1);
INSERT INTO `workshop_group` VALUES (23, 55, 2);
INSERT INTO `workshop_group` VALUES (24, 55, 1);
INSERT INTO `workshop_group` VALUES (25, 55, 4);
INSERT INTO `workshop_group` VALUES (26, 55, 5);
INSERT INTO `workshop_group` VALUES (27, 55, 3);
INSERT INTO `workshop_group` VALUES (28, 56, 3);
INSERT INTO `workshop_group` VALUES (29, 56, 1);
INSERT INTO `workshop_group` VALUES (30, 56, 2);
INSERT INTO `workshop_group` VALUES (31, 56, 4);
INSERT INTO `workshop_group` VALUES (32, 56, 6);
INSERT INTO `workshop_group` VALUES (33, 56, 5);
INSERT INTO `workshop_group` VALUES (34, 56, 7);
INSERT INTO `workshop_group` VALUES (35, 56, 12);
INSERT INTO `workshop_group` VALUES (36, 56, 13);
INSERT INTO `workshop_group` VALUES (37, 56, 9);
INSERT INTO `workshop_group` VALUES (38, 56, 11);
INSERT INTO `workshop_group` VALUES (39, 56, 10);
INSERT INTO `workshop_group` VALUES (40, 56, 8);
INSERT INTO `workshop_group` VALUES (41, 56, 14);
INSERT INTO `workshop_group` VALUES (42, 56, 17);
INSERT INTO `workshop_group` VALUES (43, 56, 18);
INSERT INTO `workshop_group` VALUES (44, 56, 15);
INSERT INTO `workshop_group` VALUES (45, 56, 19);
INSERT INTO `workshop_group` VALUES (46, 56, 16);
INSERT INTO `workshop_group` VALUES (47, 56, 20);
INSERT INTO `workshop_group` VALUES (48, 56, 21);
INSERT INTO `workshop_group` VALUES (49, 56, 25);
INSERT INTO `workshop_group` VALUES (50, 56, 23);
INSERT INTO `workshop_group` VALUES (51, 56, 22);
INSERT INTO `workshop_group` VALUES (52, 56, 24);
INSERT INTO `workshop_group` VALUES (53, 56, 26);
INSERT INTO `workshop_group` VALUES (54, 56, 27);
INSERT INTO `workshop_group` VALUES (55, 56, 31);
INSERT INTO `workshop_group` VALUES (56, 56, 30);
INSERT INTO `workshop_group` VALUES (57, 56, 29);
INSERT INTO `workshop_group` VALUES (58, 56, 28);
INSERT INTO `workshop_group` VALUES (59, 56, 32);
INSERT INTO `workshop_group` VALUES (60, 56, 36);
INSERT INTO `workshop_group` VALUES (61, 56, 37);
INSERT INTO `workshop_group` VALUES (62, 56, 35);
INSERT INTO `workshop_group` VALUES (63, 56, 38);
INSERT INTO `workshop_group` VALUES (64, 56, 33);
INSERT INTO `workshop_group` VALUES (65, 56, 34);
INSERT INTO `workshop_group` VALUES (66, 56, 39);
INSERT INTO `workshop_group` VALUES (67, 56, 42);
INSERT INTO `workshop_group` VALUES (68, 56, 44);
INSERT INTO `workshop_group` VALUES (69, 56, 41);
INSERT INTO `workshop_group` VALUES (70, 56, 40);
INSERT INTO `workshop_group` VALUES (71, 56, 43);
INSERT INTO `workshop_group` VALUES (72, 56, 45);
INSERT INTO `workshop_group` VALUES (73, 56, 46);
INSERT INTO `workshop_group` VALUES (74, 56, 49);
INSERT INTO `workshop_group` VALUES (75, 56, 48);
INSERT INTO `workshop_group` VALUES (76, 56, 50);
INSERT INTO `workshop_group` VALUES (77, 56, 47);
INSERT INTO `workshop_group` VALUES (78, 56, 51);
INSERT INTO `workshop_group` VALUES (79, 56, 52);
INSERT INTO `workshop_group` VALUES (80, 56, 53);
INSERT INTO `workshop_group` VALUES (81, 56, 54);
INSERT INTO `workshop_group` VALUES (82, 56, 55);
INSERT INTO `workshop_group` VALUES (83, 57, 1);
INSERT INTO `workshop_group` VALUES (84, 57, 2);
INSERT INTO `workshop_group` VALUES (85, 57, 4);
INSERT INTO `workshop_group` VALUES (86, 57, 3);
INSERT INTO `workshop_group` VALUES (87, 58, 1);
INSERT INTO `workshop_group` VALUES (88, 58, 2);
INSERT INTO `workshop_group` VALUES (89, 58, 3);
INSERT INTO `workshop_group` VALUES (90, 59, 1);
INSERT INTO `workshop_group` VALUES (91, 59, 3);
INSERT INTO `workshop_group` VALUES (92, 59, 2);
INSERT INTO `workshop_group` VALUES (93, 60, 1);
INSERT INTO `workshop_group` VALUES (94, 60, 2);
INSERT INTO `workshop_group` VALUES (95, 60, 5);
INSERT INTO `workshop_group` VALUES (96, 60, 3);
INSERT INTO `workshop_group` VALUES (97, 60, 4);
INSERT INTO `workshop_group` VALUES (98, 62, 1);
INSERT INTO `workshop_group` VALUES (99, 62, 3);
INSERT INTO `workshop_group` VALUES (100, 62, 4);
INSERT INTO `workshop_group` VALUES (101, 62, 5);
INSERT INTO `workshop_group` VALUES (102, 62, 2);
INSERT INTO `workshop_group` VALUES (103, 62, 6);
INSERT INTO `workshop_group` VALUES (104, 62, 7);
INSERT INTO `workshop_group` VALUES (105, 63, 4);
INSERT INTO `workshop_group` VALUES (106, 63, 3);
INSERT INTO `workshop_group` VALUES (107, 63, 2);
INSERT INTO `workshop_group` VALUES (108, 63, 1);
INSERT INTO `workshop_group` VALUES (109, 63, 5);
INSERT INTO `workshop_group` VALUES (110, 64, 1);
INSERT INTO `workshop_group` VALUES (111, 64, 5);
INSERT INTO `workshop_group` VALUES (112, 64, 2);
INSERT INTO `workshop_group` VALUES (113, 64, 4);
INSERT INTO `workshop_group` VALUES (114, 64, 3);
INSERT INTO `workshop_group` VALUES (115, 65, 1);
INSERT INTO `workshop_group` VALUES (116, 65, 2);
INSERT INTO `workshop_group` VALUES (117, 66, 3);
INSERT INTO `workshop_group` VALUES (118, 66, 2);
INSERT INTO `workshop_group` VALUES (119, 66, 4);
INSERT INTO `workshop_group` VALUES (120, 66, 1);
INSERT INTO `workshop_group` VALUES (121, 67, 4);
INSERT INTO `workshop_group` VALUES (122, 67, 1);
INSERT INTO `workshop_group` VALUES (123, 67, 2);
INSERT INTO `workshop_group` VALUES (124, 67, 3);
INSERT INTO `workshop_group` VALUES (125, 67, 5);
INSERT INTO `workshop_group` VALUES (126, 68, 1);
INSERT INTO `workshop_group` VALUES (127, 68, 5);
INSERT INTO `workshop_group` VALUES (128, 68, 3);
INSERT INTO `workshop_group` VALUES (129, 68, 2);
INSERT INTO `workshop_group` VALUES (130, 68, 4);

-- ----------------------------
-- Table structure for workshop_skill
-- ----------------------------
DROP TABLE IF EXISTS `workshop_skill`;
CREATE TABLE `workshop_skill`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workshop_id` int(11) NULL DEFAULT NULL,
  `create_by_id` bigint(20) NULL DEFAULT NULL,
  `skill_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `workshop_id`(`workshop_id`) USING BTREE,
  INDEX `skill_id`(`skill_id`) USING BTREE,
  INDEX `create_by_id`(`create_by_id`) USING BTREE,
  CONSTRAINT `workshop_skill_ibfk_1` FOREIGN KEY (`workshop_id`) REFERENCES `workshop` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `workshop_skill_ibfk_2` FOREIGN KEY (`skill_id`) REFERENCES `skill` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `workshop_skill_ibfk_3` FOREIGN KEY (`create_by_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of workshop_skill
-- ----------------------------
INSERT INTO `workshop_skill` VALUES (1, 1, 3, 1);
INSERT INTO `workshop_skill` VALUES (2, 1, 3, 2);
INSERT INTO `workshop_skill` VALUES (3, 1, 3, 3);
INSERT INTO `workshop_skill` VALUES (4, 1, 3, 4);
INSERT INTO `workshop_skill` VALUES (5, 2, 3, 5);
INSERT INTO `workshop_skill` VALUES (6, 2, 3, 6);
INSERT INTO `workshop_skill` VALUES (7, 2, 3, 7);
INSERT INTO `workshop_skill` VALUES (8, 2, 3, 8);
INSERT INTO `workshop_skill` VALUES (9, 3, 4, 1);
INSERT INTO `workshop_skill` VALUES (10, 3, 4, 2);
INSERT INTO `workshop_skill` VALUES (11, 3, 4, 3);
INSERT INTO `workshop_skill` VALUES (12, 3, 4, 4);
INSERT INTO `workshop_skill` VALUES (13, 4, 4, 1);
INSERT INTO `workshop_skill` VALUES (14, 4, 4, 2);
INSERT INTO `workshop_skill` VALUES (15, 4, 4, 3);
INSERT INTO `workshop_skill` VALUES (16, 4, 4, 4);
INSERT INTO `workshop_skill` VALUES (17, 5, 4, 5);
INSERT INTO `workshop_skill` VALUES (18, 5, 4, 6);
INSERT INTO `workshop_skill` VALUES (19, 5, 4, 7);
INSERT INTO `workshop_skill` VALUES (20, 5, 4, 8);
INSERT INTO `workshop_skill` VALUES (21, 9, 4, 1);
INSERT INTO `workshop_skill` VALUES (22, 9, 4, 2);
INSERT INTO `workshop_skill` VALUES (23, 9, 4, 3);
INSERT INTO `workshop_skill` VALUES (24, 9, 4, 4);
INSERT INTO `workshop_skill` VALUES (25, 6, 4, 8);
INSERT INTO `workshop_skill` VALUES (26, 6, 4, 1);
INSERT INTO `workshop_skill` VALUES (27, 6, 4, 5);
INSERT INTO `workshop_skill` VALUES (28, 6, 4, 6);
INSERT INTO `workshop_skill` VALUES (29, 57, 17, 4);
INSERT INTO `workshop_skill` VALUES (30, 57, 17, 5);
INSERT INTO `workshop_skill` VALUES (31, 57, 17, 7);
INSERT INTO `workshop_skill` VALUES (32, 57, 17, 1);
INSERT INTO `workshop_skill` VALUES (33, 58, 17, 1);
INSERT INTO `workshop_skill` VALUES (34, 58, 17, 2);
INSERT INTO `workshop_skill` VALUES (35, 58, 17, 4);
INSERT INTO `workshop_skill` VALUES (36, 58, 17, 3);
INSERT INTO `workshop_skill` VALUES (37, 59, 17, 6);
INSERT INTO `workshop_skill` VALUES (38, 59, 17, 5);
INSERT INTO `workshop_skill` VALUES (39, 59, 17, 1);
INSERT INTO `workshop_skill` VALUES (40, 59, 17, 4);
INSERT INTO `workshop_skill` VALUES (41, 60, 17, 1);
INSERT INTO `workshop_skill` VALUES (42, 60, 17, 3);
INSERT INTO `workshop_skill` VALUES (43, 60, 17, 4);
INSERT INTO `workshop_skill` VALUES (44, 60, 17, 2);
INSERT INTO `workshop_skill` VALUES (45, 62, 17, 1);
INSERT INTO `workshop_skill` VALUES (46, 62, 17, 2);
INSERT INTO `workshop_skill` VALUES (47, 62, 17, 3);
INSERT INTO `workshop_skill` VALUES (48, 62, 17, 5);
INSERT INTO `workshop_skill` VALUES (49, 63, 17, 1);
INSERT INTO `workshop_skill` VALUES (50, 63, 17, 2);
INSERT INTO `workshop_skill` VALUES (51, 63, 17, 3);
INSERT INTO `workshop_skill` VALUES (52, 63, 17, 4);
INSERT INTO `workshop_skill` VALUES (53, 64, 17, 2);
INSERT INTO `workshop_skill` VALUES (54, 64, 17, 1);
INSERT INTO `workshop_skill` VALUES (55, 64, 17, 4);
INSERT INTO `workshop_skill` VALUES (56, 64, 17, 5);
INSERT INTO `workshop_skill` VALUES (57, 65, 17, 3);
INSERT INTO `workshop_skill` VALUES (58, 65, 17, 4);
INSERT INTO `workshop_skill` VALUES (59, 65, 17, 1);
INSERT INTO `workshop_skill` VALUES (60, 65, 17, 2);
INSERT INTO `workshop_skill` VALUES (61, 66, 17, 1);
INSERT INTO `workshop_skill` VALUES (62, 66, 17, 3);
INSERT INTO `workshop_skill` VALUES (63, 66, 17, 2);
INSERT INTO `workshop_skill` VALUES (64, 66, 17, 4);
INSERT INTO `workshop_skill` VALUES (65, 68, 27, 3);
INSERT INTO `workshop_skill` VALUES (66, 68, 27, 2);
INSERT INTO `workshop_skill` VALUES (67, 68, 27, 4);
INSERT INTO `workshop_skill` VALUES (68, 68, 27, 1);

SET FOREIGN_KEY_CHECKS = 1;
