export class User {
  public id: number;
  public email: String;
  public firstname: String;
  public lastname: String;
  public nickname: String;
  public tel: String;

  constructor(
    id: number,
    email: String,
    firstname: String,
    lastname: String,
    nickname: String,
    tel: String
  ) {
    this.id = id;
    this.email = email;
    this.firstname = firstname;
    this.lastname = lastname;
    this.nickname = nickname;
    this.tel = tel;
  }
}
