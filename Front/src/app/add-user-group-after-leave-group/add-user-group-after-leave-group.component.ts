import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user-group-after-leave-group',
  templateUrl: './add-user-group-after-leave-group.component.html',
  styleUrls: ['./add-user-group-after-leave-group.component.css']
})
export class AddUserGroupAfterLeaveGroupComponent implements OnInit {
  workshopGroups: [];
  groupNumber: any;
  userGroup: any;
  workshopId: any;
  workshopIdForApi: any;
  studentId: any;

  addGroups: FormData = new FormData();
  groupNumber2: any;

  constructor(
    private httpclient: HttpClient,
    private fb: FormBuilder,
    private backendService: BackendService,
    private router: Router
  ) {}

  ngOnInit() {
    this.httpclient
      // .get('http://localhost:8555/api/workshop/showWorkshopGroup/2')
      .get(
        'api/workshop/showWorkshopGroup/' +
          this.backendService.getWorkshopAttId()
      )
      .subscribe(res => {
        this.workshopGroups = res as any;
      });
    this.studentId = this.backendService.getStudent().id.toString();
    this.workshopIdForApi = this.backendService.getWorkshopAttId();
    console.log('workshop id from api : ' + this.workshopIdForApi);
  }

  addUserGroup(workshopGroup) {
    this.groupNumber = workshopGroup.groupNumber.toString();
    this.groupNumber2 = workshopGroup.groupNumber;

    console.log('group number after click : ' + this.groupNumber);
    this.addGroups.append('user_id', this.studentId);
    this.addGroups.append('workshop_id', this.workshopIdForApi);
    // this.addGroups.append('user_id', '16');
    // this.addGroups.append('workshop_id', '70');
  }

  addGroup() {
    this.addGroups.append('group_number', this.groupNumber);
    // this.addGroups.append('group_number', '3');
    this.httpclient
      .post('student/addStudentGroup', this.addGroups)
      .subscribe(res => {
        // this.router.navigate(['showstudentinsamegroup'])
        this.router.navigate(['leaveorreview']);
      });
  }

  addGrouptoService() {
    this.backendService.setupGroup(this.groupNumber2);
    console.log('555 : ' + this.backendService.getGroup());
  }

  // addUserGroup(workshopGroup) {
  //   const addGroup: FormData = new FormData();
  //   this.groupNumber = workshopGroup.groupNumber.toString();
  //   console.log('group number after click : ' + this.groupNumber);
  //   addGroup.append('user_id', this.studentId);
  //   addGroup.append('workshop_id', this.workshopIdForApi);
  //   addGroup.append('group_number', this.groupNumber);

  //   this.httpclient
  //     .post('http://localhost:8555/student/addStudentGroup', addGroup)
  //     .subscribe(res => { });
  // }

  // addGrouptoService(workshopGroup) {
  //   this.backendService.setupGroup(workshopGroup.groupNumber);
  //   console.log('All Group : ' + this.backendService.getGroup());
  // }
}
