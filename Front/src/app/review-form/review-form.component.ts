import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-review-form',
  // templateUrl: './review2-form.component.html',
  template:
    `
    <div class="col-lg-12 col-12 ">
  <button (click)="toggle()" id="bt" class="btn btn-primary mb-2 mt-3" [disabled]="!this.show2">
    {{buttonName}}
  </button></div>


<ng-container *ngIf="show">
  <div class="col-lg-10 col-12 text-center" id="features">
    <div class="row justify-content-center">
      <div class="col-10">


        <div id="form-skills">
          <form [formGroup]="form">
            <div *ngFor="let skill of inputSkills; index as i;">
              <span style="display:none;">{{sID[i] = skill.skillId}}</span>
              <div class="form-group">
                <label>skill id: {{skill.skillId}}</label>
                <input type="number" class="form-control" formControlName="sk{{i}}">
                <span style="color: red;" *ngIf="form.get('sk'+ i).errors?.required"><small>Skill {{skill.skillId}}
                    required</small></span>
              </div>
            </div>

            <div class="form-group">
              <label>Feedback:</label>
              <textarea type="text" name="feedback" class="form-control" style="height:100px" maxlength="200"
                formControlName="feedback"></textarea>

            </div>
            <div class="form-group">
              <label>Suggestion:</label>
              <textarea type="text" name="sugesstion" style="height:100px" maxlength="200" class="form-control"
                formControlName="sugesstion"></textarea>

            </div>

            <div class="modal-footer">
              <div class="row">

                <div class="col-12 text-center" style="text-align: center">
                  <button type="button" class="btn btn-primary" (click)="[submitForm(),disable()]"
                    [disabled]="!form.valid"><b>Submit</b></button>&nbsp;
                </div>
              </div>
            </div>
          </form>
        </div>


      </div>
    </div>
  </div>
</ng-container>
    `,

  styleUrls: ['./review-form.component.css']
})
export class ReviewFormComponent implements OnInit {
  @Input() inputSkills: any;
  @Input() inputUser: any;
  response: any;
  form: FormGroup;
  sID: any[] = [];

  show: boolean = false;
  show2: boolean = true;
  buttonName: any = 'Review';

  constructor(private fb: FormBuilder, private http: HttpClient) {
    this.createFormGroup();
  }

  ngOnInit() { }

  onSubmit() {
    alert('Form is submit.');
  }

  createFormGroup() {
    let groupInput = {
      sk0: new FormControl('', [Validators.required, Validators.max(200)]),
      sk1: new FormControl('', [Validators.required, Validators.max(200)]),
      sk2: new FormControl('', [Validators.required, Validators.max(200)]),
      sk3: new FormControl('', [Validators.required, Validators.max(200)]),
      feedback: new FormControl(''),
      suggestion: new FormControl('')
    };

    this.form = this.fb.group(groupInput);
  }

  submitForm() {
    console.log(JSON.stringify(this.form.value));
    console.log('FVal:', this.form.value);

    let userId = this.inputUser.userId;
    let workshopId = this.inputUser.workshopId;
    let groupNumber = this.inputUser.groupNumber;
    //let groupNumber = this.backendService.getGroup();
    console.log('InputUser:', this.inputUser);
    console.log('InputSkills:', this.inputSkills);
    console.log('Group number:' + groupNumber);

    let skillArr = [
      `{"skillId":${this.sID[0]}, "skillScore":${this.form.get('sk0').value}}`,
      `{"skillId":${this.sID[1]}, "skillScore":${this.form.get('sk1').value}}`,
      `{"skillId":${this.sID[2]}, "skillScore":${this.form.get('sk2').value}}`,
      `{"skillId":${this.sID[3]}, "skillScore":${this.form.get('sk3').value}}`
    ];

    const formPost: FormData = new FormData();
    formPost.append('skill1', skillArr[0]);
    formPost.append('skill2', skillArr[1]);
    formPost.append('skill3', skillArr[2]);
    formPost.append('skill4', skillArr[3]);

    formPost.append('feedback', this.form.get('feedback').value);
    formPost.append('suggestion', this.form.get('suggestion').value);

    formPost.append('jsonData', JSON.stringify(skillArr));
    formPost.append('skIDs', JSON.stringify(this.sID));
    console.log('Skill IDs: ', this.sID);

    this.http
      .post('skills/' + userId + '/' + workshopId + '/' + groupNumber, formPost)
      .subscribe(response => {
        this.response = response as any[];
        console.log(this.response);
      });
  }

  toggle() {
    this.show = !this.show;

    // CHANGE THE NAME OF THE BUTTON.
    if (this.show) {
      this.buttonName = 'Hide';
    } else if (!this.show) {
      this.buttonName = 'Review';
    }
  }

  disable() {
    this.toggle();
    this.show2 = false;
  }
}
