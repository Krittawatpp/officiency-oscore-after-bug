import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopComponent } from './top/top.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { AddWorkshopComponent } from './add-workshop/add-workshop.component';
// import { SelectHardSkillComponent } from './select-hard-skill/select-hard-skill.component';
import { AddMetricComponent } from './add-metric/add-metric.component';
import { AddgroupComponent } from './addgroup/addgroup.component';
import { ManageWorkshopComponent } from './manage-workshop/manage-workshop.component';
import { AddUserGroupComponent } from './add-user-group/add-user-group.component';
import { StudentLoginComponent } from './student-login/student-login.component';
import { ShowUserInGroupComponent } from './show-user-in-group/show-user-in-group.component';
import { SelfSummaryComponent } from './self-summary/self-summary.component';
import { EditWorkshopComponent } from './edit-workshop/edit-workshop.component';
import { StudentRegisterComponent } from './student-register/student-register.component';
import { ReviewResultComponent } from './review-result/review-result.component';
import { ReviewInstructorComponent } from './review-instructor/review-instructor.component';
import { AddUserGroupAfterLeaveGroupComponent } from './add-user-group-after-leave-group/add-user-group-after-leave-group.component';
import { LeaveOrReviewGroupComponent } from './leave-or-review-group/leave-or-review-group.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';

const routes: Routes = [
  {
    path: '',
    component: TopComponent
  },
  {
    path: 'home',
    component: TopComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'addworkshop',
    component: AddWorkshopComponent
  },
  {
    path: 'addmetric',
    component: AddMetricComponent
  },
  {
    path: 'addgroup',
    component: AddgroupComponent
  },
  {
    path: 'manage',
    component: ManageWorkshopComponent
  },
  {
    path: 'addmetric',
    component: AddMetricComponent
  },
  {
    path: 'addusergroup',
    component: AddUserGroupComponent
  },
  {
    path: 'studentlogin/:workshopId',
    component: StudentLoginComponent
  },
  {
    path: 'showstudentinsamegroup',
    component: ShowUserInGroupComponent
  },
  {
    path: 'selfsummary',
    component: SelfSummaryComponent
  },
  {
    path: 'editworkshop',
    component: EditWorkshopComponent
  },
  {
    path: 'studentregis',
    component: StudentRegisterComponent
  },
  {
    path: 'result',
    component: ReviewResultComponent
  },
  {
    path: 'studentregister',
    component: StudentRegisterComponent
  },
  {
    path: 'reviewInstructor',
    component: ReviewInstructorComponent
  },
  {
    path: 'addusergroupafterleavegroup',
    component: AddUserGroupAfterLeaveGroupComponent
  },
  {
    path: 'leaveorreview',
    component: LeaveOrReviewGroupComponent
  },
  {
    path: 'editprofile',
    component: EditProfileComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
