import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
// import { HttpClient } from 'selenium-webdriver/http';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  users: any;
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private backendService: BackendService,
    private router: Router
  ) {
    this.form = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      loginPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ])
    });
  }

  ngOnInit() {
    this.submitForm();
  }

  onSubmit() {
    alert(JSON.stringify(this.form.value));
  }

  submitForm() {
    const login: FormData = new FormData();

    login.append('email', this.form.get('email').value);
    login.append('password', this.form.get('loginPassword').value);

    // login.append('email', 'tuu@gmail.com');
    // login.append('password', 't1234');

    this.httpClient.post('login', login).subscribe(result => {
      this.users = result as any[];
      // console.log(this.users);
      let userId = this.users['userId'];
      let email = this.users['email'];
      let firstname = this.users['firstName'];
      let lastname = this.users['lastName'];
      let nickname = this.users['nickName'];
      let tel = this.users['tel'];
      this.backendService.setupUser(
        userId,
        email,
        firstname,
        lastname,
        nickname,
        tel
      );
      console.log('login as : ' + this.backendService.getUser().email);
      // เราเตอร์ลิ้งไปหน้า profile
      this.router.navigate(['profile']);
    });
  }
}
