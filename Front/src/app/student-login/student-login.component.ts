import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../backend.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.css']
})
export class StudentLoginComponent implements OnInit {
  student: any;
  form: FormGroup;
  workshopId: any;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private backendService: BackendService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.form = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      loginPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ])
    });
  }

  ngOnInit() {
    this.route.params.subscribe(p => {
      this.workshopId = p.workshopId;
      console.log('Workshop id from param : ' + this.workshopId);
    });
    this.backendService.setWorkshopAttId(this.workshopId);
    console.log(
      'Workshop id from backend service : ' +
      this.backendService.getWorkshopAttId()
    );

    this.submitForm();
  }

  onSubmit() {
    alert(JSON.stringify(this.form.value));
  }

  submitForm() {
    const login: FormData = new FormData();

    login.append('email', this.form.get('email').value);
    login.append('password', this.form.get('loginPassword').value);

    // login.append('email', 'tuu@gmail.com');
    // login.append('password', 't12345');

    this.httpClient.post('login', login).subscribe(result => {
      this.student = result as any[];
      // console.log(this.student);
      let userId = this.student['userId'];
      let email = this.student['email'];
      let firstname = this.student['firstName'];
      let lastname = this.student['lastName'];
      let nickname = this.student['nickName'];
      let tel = this.student['tel'];
      //console.log("Debug: student-login ",this.backendService)
      this.backendService.setupStudent(
        userId,
        email,
        firstname,
        lastname,
        nickname,
        tel
      );
      console.log(
        'Login successful as : ' + this.backendService.getStudent().email
      );
      this.router.navigateByUrl('/addusergroup');
    });
  }
}
