import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewFriendsComponent } from './review-friends.component';

describe('ReviewFriendsComponent', () => {
  let component: ReviewFriendsComponent;
  let fixture: ComponentFixture<ReviewFriendsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewFriendsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewFriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
