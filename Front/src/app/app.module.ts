import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { ManageWorkshopComponent } from './manage-workshop/manage-workshop.component';
import { AddWorkshopComponent } from './add-workshop/add-workshop.component';
import { TopComponent } from './top/top.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { FeatureComponent } from './feature/feature.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HowtoComponent } from './howto/howto.component';
import { HttpClientModule } from '@angular/common/http';
import { AddMetricComponent } from './add-metric/add-metric.component';
import { BackendService } from './backend.service';
import { AddgroupComponent } from './addgroup/addgroup.component';
// import { QRCodeModule } from 'angular2-qrcode';
import { QRCodeComponent } from './qrcode/qrcode.component';
import { QRCodeGoQr } from './qrcode-goqr/qrcode-goqr.component';
import { AddUserGroupComponent } from './add-user-group/add-user-group.component';
import { ShowUserInGroupComponent } from './show-user-in-group/show-user-in-group.component';
import { ReviewFriendsComponent } from './review-friends/review-friends.component';
import { ReviewFormComponent } from './review-form/review-form.component';
import { StudentLoginComponent } from './student-login/student-login.component';
import { SelfSummaryComponent } from './self-summary/self-summary.component';
import { EditWorkshopComponent } from './edit-workshop/edit-workshop.component';
import { MaterialModule } from '../assets/material.module';
import { StudentRegisterComponent } from './student-register/student-register.component';
import { ReviewResultComponent } from './review-result/review-result.component';
import { MatConfirmDialogComponent } from './mat-confirm-dialog/mat-confirm-dialog.component';
import { DialogService } from './dialog.service';
import { MatIconModule } from '@angular/material';
import { ReviewInstructorComponent } from './review-instructor/review-instructor.component';
import { AddUserGroupAfterLeaveGroupComponent } from './add-user-group-after-leave-group/add-user-group-after-leave-group.component';
import { LeaveOrReviewGroupComponent } from './leave-or-review-group/leave-or-review-group.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';


// import { SelectHardSkillComponent } from './select-hard-skill/select-hard-skill.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,
    ManageWorkshopComponent,
    AddWorkshopComponent,
    TopComponent,
    FeatureComponent,
    HowtoComponent,
    AddMetricComponent,
    AddgroupComponent,
    QRCodeComponent,
    QRCodeGoQr,
    AddUserGroupComponent,
    ShowUserInGroupComponent,
    ReviewFriendsComponent,
    ReviewFormComponent,
    StudentLoginComponent,
    SelfSummaryComponent,
    EditWorkshopComponent,
    StudentRegisterComponent,
    ReviewResultComponent,
    MatConfirmDialogComponent,
    ReviewInstructorComponent,
    AddUserGroupAfterLeaveGroupComponent,
    LeaveOrReviewGroupComponent,
    EditProfileComponent
  ],
  imports: [
    // QRCodeModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    MatIconModule
  ],
  providers: [BackendService, DialogService],
  bootstrap: [AppComponent],
  entryComponents: [MatConfirmDialogComponent]
})
export class AppModule { }
