import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-add-metric',
  templateUrl: './add-metric.component.html',
  styleUrls: ['./add-metric.component.css']
})
export class AddMetricComponent implements OnInit {
  form: FormGroup;
  metric = ['1', '2', '3', '4', '5', '6', '7', '8'];

  groupSkill: String[] = [];
  // let groupSkill = new Set();
  selectedValue: String[] = [];

  toggleOptions: Array<String> = this.metric;
  get: any[] = [];

  constructor(
    private httpclient: HttpClient,
    private fb: FormBuilder,
    private backendService: BackendService
  ) {}

  selectionSkill() {
    this.selectedValue.forEach(i => {
      i;
    });
    this.groupSkill = this.selectedValue;

    // console.log(this.groupSkill);
    // this.get = [];
    this.get = this.groupSkill as any;
    console.log(this.get);
  }

  postSkill() {
    for (let skill of this.get) {
      const payload: FormData = new FormData();
      console.log(this.backendService.getUser().id.toString());
      payload.append(
        'create_by_id',
        this.backendService.getUser().id.toString()
      );
      payload.append(
        'workshop_id',
        this.backendService.getWorkshop().workshopId.toString()
      );
      payload.append('skill_id', skill);

      this.httpclient
        .post('api/addNewWorkshopSkill/', payload)
        .subscribe(result => {});
    }
  }

  ngOnInit() {}
}
