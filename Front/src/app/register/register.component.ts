import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
//import { HttpClient } from 'selenium-webdriver/http';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  formRegister: FormGroup;
  users: any;

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private backendService: BackendService
  ) {
    console.log('Hello,Register....');
    this.createForm();
  }

  ngOnInit() {}
  onSubmit() {
    alert(JSON.stringify(this.formRegister.value));
  }

  createForm() {
    this.formRegister = this.formBuilder.group({
      firstname: new FormControl('', [
        Validators.required,
        Validators.pattern('^[A-Za-z]{2,20}$')
      ]),
      lastname: new FormControl('', [
        Validators.required,
        Validators.pattern('^[A-Za-z]{2,20}$')
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      tel: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]{9,10}$')
      ]),
      password: new FormControl('', [Validators.required]), //TODO: Match pattern with Spring FrameWork
      cfpassword: new FormControl('', [Validators.required]), //TODO: Match pattern with Spring FrameWork
      userrole: ['']
    });
  }

  submitForm() {
    console.log('FormRegister:', this.formRegister);
    if (this.formRegister.valid) {
      alert('Valid');
      const jsonUser = JSON.stringify(this.formRegister.value);

      const data: FormData = new FormData();
      data.append('firstName', this.formRegister.get('firstname').value);
      data.append('lastName', this.formRegister.get('lastname').value);
      data.append('email', this.formRegister.get('email').value);
      data.append('tel', this.formRegister.get('tel').value);
      data.append('password', this.formRegister.get('password').value);
      data.append('confirmPassword', this.formRegister.get('cfpassword').value);
      data.append('userRole', this.formRegister.get('userrole').value);

      this.httpClient
        .post(`register/${this.formRegister.get('userrole').value}`, data) //TODO: Allow both student and Instructor to register
        .subscribe(result => {
          this.users = result as any[];
          console.log('Response: ', this.users);
          this.users['status'] == 'fail' ? alert(this.users['message']) : false;

          let userId = this.users['userId'];
          let email = this.users['email'];
          let firstname = this.users['firstName'];
          let lastname = this.users['lastName'];
          let nickname = this.users['nickName'];
          let tel = this.users['tel'];

          this.backendService.setupUser(
            userId,
            email,
            firstname,
            lastname,
            nickname,
            tel
          );
        });
    } else {
      alert('Invalid');
    }
  }
}

//Code BackUp
// firstname: ['', [Validators.required, Validators.pattern('^[A-Za-z]{2,20}$')]],
// lastname: ['', [Validators.required, Validators.pattern('^[A-Za-z]{2,20}$')]],
// email: ['', [Validators.email, Validators.required ]],
// tel: ['', [Validators.required, Validators.pattern('^[0-9]{9,10}$')]],
// password: ['', [Validators.required, Validators.pattern('[a-zA-z0-9]{4,}')]],//Minimum 4 characters, at least one letter and one number
// confirmpassword: ['', [Validators.required, Validators.pattern('[a-zA-z0-9]{4,}')]],
// userrole: ['']
