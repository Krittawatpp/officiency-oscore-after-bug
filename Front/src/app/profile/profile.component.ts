import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Workshop } from '../models/workshop';
import { BackendService } from '../backend.service';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  // gg: AppComponent;
  workshops: Workshop[] = [];
  // user: any;
  workshop: any[];
  form: FormGroup;
  workshopAtts: any[];

  valueTwo: number = 100;

  constructor(
    private httpclient: HttpClient,
    private fb: FormBuilder,
    public backendService: BackendService
  ) {
    this.form = this.fb.group({
      userId: '',
      firstName: '',
      lastName: '',
      email: '',
      gender: ''
    });
  }

  ngOnInit() {
    this.loadWorkshop();
    this.loadScore();
    this.loadWorkshopAtt();
  }

  loadScore() {
    this.workshop = [];
    this.httpclient
      .get('api/getAllUserScoreById/' + this.backendService.getUser().id)
      .subscribe(result => {
        this.workshop = result as any[];
        // alert(JSON.stringify(result));
        // this.user = JSON.stringify(this.users);
      });
  }

  loadWorkshopAtt() {
    this.workshopAtts = [];
    this.httpclient
      .get(
        'api/getWorkshopAttToProfileById/' + this.backendService.getUser().id
      )
      .subscribe(res => {
        this.workshopAtts = res as any;
      });
    // console.log(this.backendService.getUser());
  }

  loadWorkshop() {
    // this.workshop = [];
    this.httpclient
      .get<Workshop[]>('api/getWorkShops/' + this.backendService.getUser().id)
      .subscribe(result => {
        this.workshops = result;
        // alert(JSON.stringify(result));
        // this.user = JSON.stringify(this.users);
      });
  }
}
