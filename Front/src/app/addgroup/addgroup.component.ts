import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-addgroup',
  templateUrl: './addgroup.component.html',
  styleUrls: ['./addgroup.component.css']
})
export class AddgroupComponent implements OnInit {
  form: FormGroup;
  groups: any[] = [];
  workshopId: any;

  constructor(
    private httpclient: HttpClient,
    private fb: FormBuilder,
    private backendService: BackendService
  ) {
    this.form = this.fb.group({
      groupNumber: ''
    });
  }

  ngOnInit() { }

  loopTextbox() {
    this.groups = [];
    for (let i = 1; i <= this.form.value.groupNumber; i++) {
      this.groups.push(i);
    }

    console.log(this.groups);
  }

  submitForm() {
    for (let k = 1; k <= this.form.value.groupNumber; k++) {
      const payload: FormData = new FormData();

      let numGroup = String(k);
      this.workshopId = this.backendService.getWorkshop().workshopId;
      payload.append('workshop_id', this.workshopId);
      payload.append('group_number', numGroup);

      this.httpclient
        .post('api/addNewWorkshopGroup/', payload)
        .subscribe(result => {
          // alert(numGroup);
        });
    }
  }
}
