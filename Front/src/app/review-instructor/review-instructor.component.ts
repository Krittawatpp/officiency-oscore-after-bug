import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { BackendService } from '../backend.service';
import { Workshop } from '../models/workshop';
import { Router } from '@angular/router';

@Component({
  selector: 'app-review-instructor',
  templateUrl: './review-instructor.component.html',
  styleUrls: ['./review-instructor.component.css']
})
export class ReviewInstructorComponent implements OnInit {
  @Input()
  skillsForReview: []; // Instructor's skills to be reviewed
  workshop: Workshop; //workshopId, createById, title, description,..
  workshopId: any; //Id of current workshop being reviewed by students
  instructor: any; //Instructor who create this workshop
  groupNumber: number; //Student's group who giving review for his/her instructor
  form: FormGroup;
  sID: any[] = [];

  constructor(
    private fb: FormBuilder,
    private httpclient: HttpClient,
    private backendService: BackendService,
    private router: Router
  ) {
    this.workshopId = this.backendService.getWorkshopAttId();
    this.groupNumber = this.backendService.getGroup();
    let error = !this.workshopId || !this.groupNumber ? true : false;
    if (error == true)
      throw new Error(
        `WorkshopId and GroupNumber is required! workshopId: ${
        this.workshopId
        } groupNumber: ${this.groupNumber}`
      );
    this.setInstructor();
    this.setWorkshop();
  }

  ngOnInit() {
    this.setSkillForReview();
    this.setFormControlGroup();
  }

  setWorkshop() {
    this.httpclient
      .get('api/getWorkshopByWorkshopId/' + this.workshopId)
      .subscribe(res => {
        this.workshop = res as any;
        console.log('ReviewInstructor -> WORKSHOP: ', this.workshop);
      });
  }

  setInstructor() {
    this.workshopId = this.backendService.getWorkshopAttId();
    this.groupNumber = this.backendService.getGroup();
    let error = !this.workshopId || !this.groupNumber ? true : false;
    if (error == true)
      throw new Error(
        `WorkshopId and Group is required! workshopId: ${
        this.workshopId
        } group: ${this.groupNumber}`
      );

    this.httpclient
      .get('api/getUserByWorkshopId/' + this.workshopId)
      .subscribe(res => {
        this.instructor = {};
        this.instructor = res as any;
        this.instructor['worshopId'] = this.workshopId;
        this.instructor['groupNumber'] = this.groupNumber;
        console.log('ReviewInstructor -> INSTRUCTOR: ', this.instructor);
      });
  }

  setSkillForReview() {
    ///api/getAllInstructorSkills
    this.httpclient.get('api/getAllInstructorSkills').subscribe(res => {
      this.skillsForReview = res as any;
      console.log('*** SkillsForReview: ', this.skillsForReview);
    });
  }

  setFormControlGroup() {
    let groupInput = {
      sk0: new FormControl('', [Validators.required, Validators.max(200)]),
      sk1: new FormControl('', [Validators.required, Validators.max(200)]),
      sk2: new FormControl('', [Validators.required, Validators.max(200)]),
      sk3: new FormControl('', [Validators.required, Validators.max(200)]),
      sk4: new FormControl('', [Validators.required, Validators.max(200)]),
      sk5: new FormControl('', [Validators.required, Validators.max(200)]),
      feedback: new FormControl('', []),
      suggestion: new FormControl('', [])
    };
    this.form = this.fb.group(groupInput);
  }

  submitForm() {
    console.log(JSON.stringify(this.form.value));
    console.log('FVal:', this.form.value);

    let skillArr = [
      `{"skillId":${this.sID[0]}, "skillScore":${this.form.get('sk0').value}}`,
      `{"skillId":${this.sID[1]}, "skillScore":${this.form.get('sk1').value}}`,
      `{"skillId":${this.sID[2]}, "skillScore":${this.form.get('sk2').value}}`,
      `{"skillId":${this.sID[3]}, "skillScore":${this.form.get('sk3').value}}`,
      `{"skillId":${this.sID[4]}, "skillScore":${this.form.get('sk4').value}}`,
      `{"skillId":${this.sID[5]}, "skillScore":${this.form.get('sk5').value}}`
    ];

    const formPost: FormData = new FormData();
    formPost.append('skill1', skillArr[0]);
    formPost.append('skill2', skillArr[1]);
    formPost.append('skill3', skillArr[2]);
    formPost.append('skill4', skillArr[3]);
    formPost.append('skill5', skillArr[4]);
    formPost.append('skill6', skillArr[5]);

    formPost.append('feedback', this.form.get('feedback').value);
    formPost.append('suggestion', this.form.get('suggestion').value);

    formPost.append('jsonData', JSON.stringify(skillArr));
    formPost.append('skIDs', JSON.stringify(this.sID));
    console.log('Skill IDs: ', this.sID);

    this.httpclient
      .post(
        'saveInstructorSkillScore/' +
        this.instructor.userId +
        '/' +
        this.workshopId +
        '/' +
        this.groupNumber,
        formPost
      )
      .subscribe(response => {
        console.log('Response: ', response);
      });
    this.router.navigateByUrl('/leaveorreview');
  }
}
