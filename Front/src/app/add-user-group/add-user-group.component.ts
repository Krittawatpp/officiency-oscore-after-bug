import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user-group',
  templateUrl: './add-user-group.component.html',
  styleUrls: ['./add-user-group.component.css']
})
export class AddUserGroupComponent implements OnInit {
  workshopGroups: [];
  groupNumber: any;
  userGroup: any;
  workshopId: any;
  workshopIdForApi: any;
  studentId: any;
  addGroups: FormData = new FormData();

  groupNumber2: any;

  constructor(
    private httpclient: HttpClient,
    private backendService: BackendService,
    private router: Router
  ) { }

  ngOnInit() {
    this.httpclient
      .get(
        'api/workshop/showWorkshopGroup/' +
        this.backendService.getWorkshopAttId()
      )
      .subscribe(res => {
        this.workshopGroups = res as any;
      });

    this.studentId = this.backendService.getStudent().id.toString();
    this.workshopIdForApi = this.backendService.getWorkshopAttId();
    console.log('workshop id from api : ' + this.workshopIdForApi);
    // this.studentId = this.backendService.getStudent().id;
    // console.log('add group as id : ' + this.studentId);
  }

  addUserGroup(workshopGroup) {
    this.groupNumber = workshopGroup.groupNumber.toString();
    this.groupNumber2 = workshopGroup.groupNumber;

    console.log('group number after click : ' + this.groupNumber);
    this.addGroups.append('user_id', this.studentId);
    this.addGroups.append('workshop_id', this.workshopIdForApi);
    // this.addGroups.append('user_id', '16');
    // this.addGroups.append('workshop_id', '70');
  }

  addGroup() {
    this.addGroups.append('group_number', this.groupNumber);
    // this.addGroups.append('group_number', '3');
    this.httpclient
      .post('student/addStudentGroup', this.addGroups)
      .subscribe(res => {
        // this.router.navigate(['showstudentinsamegroup'])
        this.router.navigate(['reviewInstructor']);
      });
  }

  addGrouptoService() {
    this.backendService.setupGroup(this.groupNumber2);
    console.log('555 : ' + this.backendService.getGroup());
  }

  // onDelete($key) {
  //   this.dialogService.openConfirmDialog("Are you sure you want to join group ?");
  // }
}
