package boot.entity;

public class RegistrationRequest {

    private String email;
    private String firstname;
    private String lastname;
    private String nickname;
    private String tel;
    private String password;
    private String confirmPassword;

    public RegistrationRequest() {
    }

    public RegistrationRequest(String email, String firstname, String lastname, String nickname, String tel,
            String password, String confirmPassword) {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.nickname = nickname;
        this.tel = tel;
        this.password = password;
        this.confirmPassword = confirmPassword;

    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return this.confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

}