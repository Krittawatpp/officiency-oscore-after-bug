package boot.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_group")
public class UserInGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = true)
    private Long id;
    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Column(name = "workshop_id", nullable = false)
    private Long workshopId;
    @Column(name = "group_number", nullable = false)
    private int groupNumber;

    public UserInGroup() {

    }

    public UserInGroup(Long userId, Long workshopId, int groupNumber) {
        this.workshopId = workshopId;
        this.groupNumber = groupNumber;
        this.userId = userId;
    }

}