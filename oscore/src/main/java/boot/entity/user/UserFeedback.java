package boot.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_feedback")
public class UserFeedback {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = true)
    private Long id;
    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Column(name = "workshop_attend_id", nullable = false)
    private Long workshopAttId;
    @Column(name = "group_number", nullable = false)
    private Long groupNumber;
    @Column(name = "feedback", nullable = false)
    private String feedback;
    @Column(name = "suggestion", nullable = false)
    private String suggestion;

    public UserFeedback() {

    }

    public UserFeedback(Long userId, Long workshopAttId, Long groupNumber, String feedback, String suggestion) {
        this.userId = userId;
        this.workshopAttId = workshopAttId;
        this.groupNumber = groupNumber;
        this.feedback = feedback;
        this.suggestion = suggestion;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWorkshopAttId() {
        return this.workshopAttId;
    }

    public void setWorkshopAttId(Long workshopAttId) {
        this.workshopAttId = workshopAttId;
    }

    public Long getGroupNumber() {
        return this.groupNumber;
    }

    public void setGroupNumber(Long groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getFeedback() {
        return this.feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getSuggestion() {
        return this.suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

}