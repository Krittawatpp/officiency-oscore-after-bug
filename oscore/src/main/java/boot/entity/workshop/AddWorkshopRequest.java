package boot.entity.workshop;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class AddWorkshopRequest {
    private Long createbyid;
    private String title;
    private String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date start;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date end;

    public AddWorkshopRequest() {

    }

    public AddWorkshopRequest(Long cratebyid, String title, String description, Date start, Date end) {
        this.createbyid = cratebyid;
        this.title = title;
        this.description = description;
        this.start = start;
        this.end = end;
    }

    public Long getCreatebyid() {
        return this.createbyid;
    }

    public void setCreatebyid(Long createbyid) {
        this.createbyid = createbyid;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStart() {
        return this.start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return this.end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

}