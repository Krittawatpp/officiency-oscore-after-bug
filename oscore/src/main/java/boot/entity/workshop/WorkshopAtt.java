package boot.entity.workshop;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_att_workshop")
public class WorkshopAtt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Column(name = "workshop_att_id", nullable = false)
    private Long attId;
    @Column(name = "group_id", nullable = false)
    private Long groupId;
    @Column(name = "skill_id", nullable = true)
    private Long skillId;
    @Column(name = "skill_score", nullable = true)
    private Long skillScore;

    public WorkshopAtt() {
    }

    public WorkshopAtt(Long userId, Long attId, Long groupId, Long skillId, Long skillScore) {
        this.userId = userId;
        this.attId = attId;
        this.groupId = groupId;
        this.skillId = skillId;
        this.skillScore = skillScore;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAttId() {
        return this.attId;
    }

    public void setAttId(Long attId) {
        this.attId = attId;
    }

    public Long getGroupId() {
        return this.groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getSkillId() {
        return this.skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    public Long getSkillScore() {
        return this.skillScore;
    }

    public void setSkillScore(Long skillScore) {
        this.skillScore = skillScore;
    }

}