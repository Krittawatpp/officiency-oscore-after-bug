package boot.repository;

import org.springframework.data.repository.CrudRepository;

import boot.entity.user.UserRole;



public interface UserRoleRepository extends CrudRepository<UserRole, Long>  {

}
