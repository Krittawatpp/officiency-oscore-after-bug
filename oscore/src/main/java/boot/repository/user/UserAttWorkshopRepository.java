package boot.repository.user;

import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.user.UserAttWorkshop;

public interface UserAttWorkshopRepository extends CrudRepository<UserAttWorkshop, Long> {

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO user_att_workshop (user_id, workshop_att_id, group_number, skill_id, skill_score) VALUES (?1, ?2, ?3, ?4, ?5);", nativeQuery = true)
    public int insertUserAttWorkshopScore(Long userId, Long workshopAttId, Long groupNumber, Long skillId,
            Long skillScore);

}