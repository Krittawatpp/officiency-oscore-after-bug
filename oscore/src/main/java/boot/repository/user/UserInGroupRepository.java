package boot.repository.user;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import boot.entity.user.UserInGroup;

public interface UserInGroupRepository extends CrudRepository<UserInGroup, Long> {

    String sqlLeaveGroup = "DELETE FROM user_group " + "WHERE user_id = ?1 " + "AND workshop_id = ?2";

    @Transactional
    @Modifying
    @Query(value = sqlLeaveGroup, nativeQuery = true)
    public int leaveGroup(Long userId, Long workshopId);

}