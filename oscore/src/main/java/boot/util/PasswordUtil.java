package boot.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordUtil {

	public String encryptPassword(String password) {
		//TODO: Add encryptPassword Here
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
       return encoder.encode(password);
	}

}