$(document).ready(function () {
    $("#ppl").change(function () {

        // The easiest way is of course to delete all textboxes before adding new ones
        //$("#holder").html("");

        var count = $("#holder input").size();
        var requested = parseInt($("#ppl").val(), 10);

        if (requested > count) {
            for (i = count; i < requested; i++) {
                let k = i + 1;
                var $ctrl = $('<input/>').attr({
                    type: 'text', name: 'text', value: k, formControlName: 'G' + k, class: 'form-control',
                    style: 'border:2px solid #97b9f8;height: 50px;border-radius: 6px;'
                });
                // console.log()
                $("#holder").append($ctrl).append("<br>");
            }
        }
        else if (requested < count) {
            var x = requested - 1;
            $("#holder input:gt(" + x + ")").remove();
        }
    });
});
