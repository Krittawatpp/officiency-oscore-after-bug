-- User role
CREATE TABLE user_role
(
    id INT
    auto_increment,
    role_name VARCHAR
    (50) NOT NULL,
    PRIMARY KEY
    (id)
);

    -- User table
    CREATE TABLE user
    (
        id INT
        auto_increment,
    first_name VARCHAR
        (40) NOT NULL,
    last_name VARCHAR
        (40) NOT NULL,
    email VARCHAR
        (50) NOT NULL,
    password VARCHAR
        (50) NOT NULL,
    user_role_id int NOT NULL,
    PRIMARY KEY
        (id),
    UNIQUE
        (first_name, last_name),
    FOREIGN KEY
        (user_role_id) REFERENCES user_role
        (id)
);

        -- Workshop table
        CREATE TABLE workshop
        (
            id INT
            auto_increment,
    title VARCHAR
            (20) NOT NULL,
    description VARCHAR
            (1200) NOT NULL,
    start DATETIME NOT null,
            end DATETIME NOT NULL,
    PRIMARY KEY
            (id),
    UNIQUE
            (title)
);

            -- workshop_score
            CREATE table workshop_has_score
            (
                workshop_id int,
                score_1 BIT,
                score_2 BIT,
                score_3 BIT,
                score_4 BIT,
                score_5 BIT,
                score_6 BIT,
                score_7 BIT,
                score_8 BIT,
                FOREIGN KEY (workshop_id) REFERENCES workshop (id)
            );

            -- workshop_group
            CREATE TABLE workshop_group
            (
                group_id INT
                auto_increment,
    workshop_id int NOT NULL,
    group_name VARCHAR
                (20) NOT NULL,
                PRIMARY key
                (group_id),
            
    FOREIGN key
                (workshop_id) REFERENCES workshop
                (id)
);

                -- User record tabble
                CREATE TABLE user_record
                (
                    user_id INT,
                    workshop_attend_id INT,
                    group_id int,
                    score_1 int,
                    score_2 int,
                    score_3 int,
                    score_4 int,
                    score_5 int,
                    score_6 int,
                    score_7 int,
                    score_8 int,
                    feedback VARCHAR(1200),
                    suggestion VARCHAR(1200),
                    FOREIGN KEY (user_id)
REFERENCES user (id),
                    FOREIGN KEY (workshop_attend_id)
REFERENCES workshop (id),
                    FOREIGN KEY (group_id) REFERENCES workshop_group (group_id)
                );

                -- user feedback
                CREATE table user_feedback
                (
                    user_id INT,
                    workshop_attend_id INT,
                    feedback VARCHAR (1200),
                    suggestion VARCHAR (1200),
                    FOREIGN KEY (user_id)
REFERENCES user (id),
                    FOREIGN KEY (workshop_attend_id)
REFERENCES workshop (id)
                );

                -- test run
                SELECT *
                FROM user_record
                WHERE workshop_attend_id = 1;

                SELECT user_id, sum(score_1), sum(score_2), sum(score_3), sum(score_
                4) from user_record  GROUP by user_id;

                SELECT user_id, sum(score_1)
                from user_record
                group by user_id;
