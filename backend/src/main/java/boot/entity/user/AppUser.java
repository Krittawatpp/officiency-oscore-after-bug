package boot.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "user" , uniqueConstraints = {@UniqueConstraint(name = "user_uk", columnNames = "email")} )
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Column(name = "email", length = 255, nullable = false)
    private String email;
    @Column(name = "password", length = 70, nullable = false)
    private String password;
    @Column(name = "firstname", length = 255, nullable = true)
    private String firstName;
    @Column(name = "lastname", length = 255, nullable = true)
    private String lastName;
    @Column(name = "nickname", length = 255, nullable = true)
    private String nickName;
    @Column(name = "tel", length = 255, nullable = true)
    private String tel;
    @Column(name = "enabled", length = 128, nullable = false)
    @JsonIgnore
    private Boolean enabled;

    public AppUser() {

    }

    public AppUser(String email, String password, String firstName, String lastName, String tel) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
        this.enabled = true;
    }

    public AppUser(String email, String password, String userRole) {
        this.email = email;
        this.password = password;
        this.enabled = true;
    }

    public AppUser(String email, String firstName, String lastName, String tel, String encryptPassword, String userRole) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
        this.password = encryptPassword;
        this.enabled = true;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}