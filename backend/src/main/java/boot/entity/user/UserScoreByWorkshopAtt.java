package boot.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UserScoreByWorkshopAtt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Column(name = "user_id", nullable = false)
    private int userId;
    // @Column(name = "workshop_att_id", nullable = false)
    // private int workshop_att_id;
    @Column(name = "skill_name", nullable = false)
    private String skillName;
    @Column(name = "avgscore", nullable = false)
    private int avgScore;

    private UserScoreByWorkshopAtt() {

    }

    private UserScoreByWorkshopAtt(int id, int userId, int workshop_att_id, String skillName, int avgScore) {
        this.id = id;
        this.userId = userId;
        // this.workshop_att_id = workshop_att_id;
        this.skillName = skillName;
        this.avgScore = avgScore;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    // public int getWorkshop_att_id() {
    // return this.workshop_att_id;
    // }

    // public void setWorkshop_att_id(int workshop_att_id) {
    // this.workshop_att_id = workshop_att_id;
    // }

    public String getSkillName() {
        return this.skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public int getAvgScore() {
        return this.avgScore;
    }

    public void setAvgScore(int avgScore) {
        this.avgScore = avgScore;
    }

}