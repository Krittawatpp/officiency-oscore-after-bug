package boot.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "instructor_score")
public class InstructorAttWorkshop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Column(name = "workshop_id", nullable = false)
    private Long workshopId;

    @Column(name = "skill_id", nullable = false)
    private Long skillId;
    @Column(name = "skill_score", nullable = false)
    private Long skillScore;

    public InstructorAttWorkshop() {

    };

    public InstructorAttWorkshop(Long userId, Long workshopId, Long skillId, Long skillScore) {
        this.userId = userId;
        this.workshopId = workshopId;
        this.skillId = skillId;
        this.skillScore = skillScore;
    };


    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWorkshopId() {
        return this.workshopId;
    }

    public void setWorkshopId(Long workshopId) {
        this.workshopId = workshopId;
    }


    public Long getSkillId() {
        return this.skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    public Long getSkillScore() {
        return this.skillScore;
    }

    public void setSkillScore(Long skillScore) {
        this.skillScore = skillScore;
    }

}