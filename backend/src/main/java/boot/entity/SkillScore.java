package boot.entity;



public class SkillScore {

    private Long skillId;
    private String skillScore;

    public SkillScore() {

    }

    public SkillScore(Long skillId, String skillScore) {
        this.skillId = skillId;
        this.skillScore = skillScore;
    }

    public Long getSkillId() {
        return this.skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    public String getSkillScore() {
        return this.skillScore;
    }

    public void setSkillScore(String skillScore) {
        this.skillScore = skillScore;
    }
}