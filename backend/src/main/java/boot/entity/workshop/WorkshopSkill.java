package boot.entity.workshop;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "workshop_skill")
public class WorkshopSkill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = true)
    private int id;
    @Column(name = "create_by_id", nullable = false)
    private int userId;
    @Column(name = "workshop_id", nullable = false)
    private int workshopId;
    @Column(name = "skill_id", nullable = false)
    private int skillId;

    public WorkshopSkill() {

    }

    public WorkshopSkill(int userId, int workshopId, int skillId) {
        this.userId = userId;
        this.workshopId = workshopId;
        this.skillId = skillId;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getWorkshopId() {
        return this.workshopId;
    }

    public void setWorkshopId(int workshopId) {
        this.workshopId = workshopId;
    }

    public int getSkillId() {
        return this.skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

}