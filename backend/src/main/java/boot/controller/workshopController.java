package boot.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import boot.entity.workshop.Workshop;
import boot.entity.workshop.WorkshopAtt;
import boot.entity.workshop.WorkshopAttToProfile;
import boot.entity.workshop.WorkshopGroup;
import boot.entity.workshop.WorkshopSkill;
import boot.entity.workshop.WorkshopSkillForReview;
import boot.service.workshop.WorkshopAttService;
import boot.service.workshop.WorkshopAttToProfileService;
import boot.service.workshop.WorkshopGroupService;
import boot.service.workshop.WorkshopService;
import boot.service.workshop.WorkshopSkillForReviewService;
import boot.service.workshop.WorkshopSkillService;

@RestController
@CrossOrigin
public class workshopController {

    @Autowired
    private WorkshopService workshopService;
    @Autowired
    private WorkshopAttService workshopAttService;
    @Autowired
    private WorkshopAttToProfileService workshopAttToProfileService;
    @Autowired
    private WorkshopSkillService workshopSkillService;
    @Autowired
    private WorkshopGroupService workshopGroupService;
    @Autowired
    private WorkshopSkillForReviewService workshopSkillForReviewService;

    @RequestMapping(value = "/api/getWorkshopByWorkshopId/{workshop_id}", method = RequestMethod.GET)
    @ResponseBody
    Workshop getInstructroByWorkshopId(@PathVariable("workshop_id") Long workshopId) {
        return workshopService.findWorkshopByWorkshopId(workshopId);
    }

    @RequestMapping(value = "api/getAllWorkshopAtt", method = RequestMethod.GET)
    @ResponseBody
    Collection<WorkshopAtt> getAllWorkshopAtt() {
        Collection<WorkshopAtt> allWorkshopAtt = workshopAttService.findAllWorkshopAtt();
        System.out.println(allWorkshopAtt.toArray());
        return allWorkshopAtt;
    }

    @RequestMapping(value = "api/getAllWorkshopAttById/{user_id}", method = RequestMethod.GET)
    @ResponseBody
    Collection<WorkshopAtt> getWorkshopAttByCreateId(@PathVariable("user_id") Long myId) {
        try {
            Collection<WorkshopAtt> allWorkshopAtt = workshopAttService.findWorkshopAttById(myId);
            return allWorkshopAtt;
        } catch (java.util.NoSuchElementException exception) {
            return null;
        }
    }

    // Done Phase II by 'AngryCode'
    // For admin tools in future
    // แสดง workshop ทั้งหมดในระบบ
    @RequestMapping(value = "api/getAllWorkshops", method = RequestMethod.GET)
    @ResponseBody
    Iterable<Workshop> getWorkShop() {
        return workshopService.findAllWorkShops();
    }

    // Done!! // CHECK!
    // แสดง workshop ที่ id นั้นๆเคยสอน by 'AngryCode'
    // แสดง workshop ที่สอน ใน profile
    @RequestMapping(value = "api/getWorkShops/{createId}", method = RequestMethod.GET)
    @ResponseBody
    Iterable<Workshop> getWorkShop(@PathVariable("createId") Long id) {
        Iterable<Workshop> workshops = workshopService.findWorkShopByCreatedId(id);
        // System.out.println("Find Workshop By Create ID....");
        return workshops;
    }

    // DONE! // CHECK!
    // แสดง workshop ที่ id นั้นๆเคยเข้าร่วม by 'P'
    // ใช้แสดง workshop att ใน profile
    @RequestMapping(value = "api/getWorkshopAttToProfileById/{user_id}", method = RequestMethod.GET)
    @ResponseBody
    List<WorkshopAttToProfile> getWorkshopAttToProfileById(@PathVariable("user_id") Long myId) {
        try {
            return workshopAttToProfileService.findWorkshopAttToProfile(myId);
        } catch (java.util.NoSuchElementException exception) {
            return null;
        }
    }

    // Done!! // CHECK!
    // Create New Workshop by 'P'
    // ใช้กับหน้า 1 ของ add workshop
    @RequestMapping(value = "/api/addNewWorkshop", method = RequestMethod.POST)
    ResponseEntity<Workshop> addAllWorkshopAndDetail(@RequestParam("create_by_id") Long userId,
            @RequestParam("title") String title, @RequestParam("daystart") String start) {
        Workshop workshop;
        ResponseEntity<Workshop> responseWorkshop;
        int addNewWorkshop;
        try {
            addNewWorkshop = workshopService.addWorkshop(userId, title, start);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {
            workshop = workshopService.findWorkshopByCreateIdandTitle(userId, title);
            responseWorkshop = new ResponseEntity<>(workshop, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        return responseWorkshop;
    }

    // Done!! // CHECK
    // Add workshop skill by'P'
    // ใช้กับหน้า 2 ของ add workshop
    @RequestMapping(value = "/api/addNewWorkshopSkill", method = RequestMethod.POST)
    @ResponseBody
    ResponseEntity<String> addWorkshopSkill(@RequestParam("create_by_id") int userId,
            @RequestParam("workshop_id") int workshopId, @RequestParam("skill_id") int skillId) {
        int workshopSkill;
        try {
            workshopSkill = workshopSkillService.addWorkshopSkill(userId, workshopId, skillId);
        } catch (Exception ex) {
            return new ResponseEntity<>("{\"status\" : \"fail\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("{\"status\" : \"success\", \"updated_row\" : " + workshopSkill + "}",
                HttpStatus.OK);
    }

    // Done!! // CHECK!!
    // Add workshop Group by 'P'
    // ใช้กับหน้า 3 ของ add workshop
    @RequestMapping(value = "/api/addNewWorkshopGroup", method = RequestMethod.POST)
    @ResponseBody
    ResponseEntity<String> addWorkshopSkill(@RequestParam("workshop_id") Long workshopId,
            @RequestParam("group_number") Long groupNumber) {
        int workshopGroup;
        try {
            workshopGroup = workshopGroupService.addWorkshopGroup(workshopId, groupNumber);
        } catch (Exception ex) {
            return new ResponseEntity<>("{\"status\" : \"fail\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("{\"status\" : \"success\", \"updated_row\" : " + workshopGroup + "}",
                HttpStatus.OK);
    }

    // Done!! by 'P'
    // Edit Workshop
    // ใช้กับ method edit ในหน้า manage workshop
    @RequestMapping(path = "/workshop/editWorkshop", method = RequestMethod.POST)
    public ResponseEntity<String> editWorkshop(@RequestParam("workshopId") Long id, @RequestParam("title") String title,
            @RequestParam("description") String desc, @RequestParam("start") String start,
            @RequestParam("end") String end) {
        int workshop;

        try {
            workshop = workshopService.editWorkshop(id, title, desc, start, end);
        } catch (Exception ex) {
            return new ResponseEntity<>("{\"status\" : \"fail\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("{\"status\" : \"success\", \"updated_row\" : " + workshop + "}", HttpStatus.OK);
    }

    // 'P'
    // Delete Workshop
    // ใช้กับ method delete ในหน้า manage workshop
    @RequestMapping(path = "/workshop/deleteWorkshop", method = RequestMethod.POST)
    public ResponseEntity<String> deleteWorkshop(@RequestParam("workshopId") Long id) {
        int workshop;
        try {
            workshop = workshopService.deleteWorkshop(id);
        } catch (Exception ex) {
            return new ResponseEntity<>("{\"status\" : \"" + ex.getMessage() + "\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("{\"status\" : \"success\", \"updated_row\" : " + workshop + "}", HttpStatus.OK);
    }

    // 'P'
    // แสดง workshop skill
    // ใช้กับหน้า review friends
    @RequestMapping(value = "workshoptoreview/{workshop_id}", method = RequestMethod.GET)
    @ResponseBody
    Collection<WorkshopSkillForReview> getWorkshopSkillByWorkshopId(@PathVariable("workshop_id") Long workshopId) {
        return workshopSkillForReviewService.findWorkshopSkillByWorkshopId(workshopId);
    };

    // Done!! by 'P'
    // show workshop group
    // ใช้กับหน้า เลือกกลุ่ม หลัง student login เข้ามา
    @RequestMapping(value = "api/workshop/showWorkshopGroup/{workshop_id}", method = RequestMethod.GET)
    @ResponseBody
    Collection<WorkshopGroup> showWorkshopGroups(@PathVariable("workshop_id") Long workshopId) {
        return workshopGroupService.showWorkshopGroup(workshopId);
    }
}