package boot.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.cj.xdevapi.JsonArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties.Lettuce;
import org.springframework.boot.json.JsonParseException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import boot.entity.Skill;
import boot.entity.SkillScore;
import boot.repository.SkillRepository;
import boot.service.SkillService;
import boot.service.user.InstructorService;
import boot.service.user.UserAttWorkshopService;
import boot.service.user.UserFeedbackService;

@RestController
@CrossOrigin
public class skillController {
    @Autowired
    private SkillService skillService;
    @Autowired
    private UserAttWorkshopService userAttWorkshopService;
    @Autowired
    private InstructorService instructorService;
    @Autowired
    private UserFeedbackService userFeedbackService;

    // ใช้ดึงข้อมูล skill ทั้งหมด
    @RequestMapping(value = "api/getAllSkill", method = RequestMethod.GET)
    Collection<Skill> getAllSkill() {
        return skillService.findAllSkill();
    }

    // On REVIEW INSTRUCTOR page, Student make review for instructor.
    @RequestMapping(value = "/saveInstructorSkillScore/{instructor_id}/{workshop_id}/{group_number}", method = RequestMethod.POST)
    void saveInstructorSkillScore(@PathVariable("instructor_id") Long instructorId,
            @PathVariable("workshop_id") Long workshopId, @PathVariable("group_number") Long groupNumber,
            @RequestParam("skill1") String skill1, @RequestParam("skill2") String skill2,
            @RequestParam("skill3") String skill3, @RequestParam("skill4") String skill4,
            @RequestParam("skill5") String skill5, @RequestParam("skill6") String skill6,
            @RequestParam("feedback") String feedback, @RequestParam("suggestion") String suggestion,
            @RequestParam("jsonData") String jsonData, @RequestParam("skIDs") String skIds)
            throws com.fasterxml.jackson.core.JsonParseException, JsonMappingException, IOException {

        System.err.println("SkillController: InstructorId:" + instructorId + " workshopId:" + workshopId);
        System.err.println("Skill1 " + skill1);
        System.err.println("Feedback: " + feedback + " Suggestion:" + suggestion);
        System.err.println("JsonData " + jsonData);
        System.err.println("Skill List:" + skIds);

        boolean insertSuccess = false;
        ArrayList<String> allSkills = new ArrayList<String>();
        allSkills.add(skill1);
        allSkills.add(skill2);
        allSkills.add(skill3);
        allSkills.add(skill4);
        allSkills.add(skill5);
        allSkills.add(skill6);

        for (int i = 0; i < allSkills.size(); i++) {
            ObjectMapper mapper = new ObjectMapper();
            SkillScore skScore = mapper.readValue(allSkills.get(i), SkillScore.class);
            System.out.println(
                    "************Skill ID: " + skScore.getSkillId() + " Skill Score: " + skScore.getSkillScore());
            int result = instructorService.insertInstructorScore(instructorId, workshopId, skScore.getSkillId(),
                    Long.parseLong(skScore.getSkillScore()));
        }
        // TODO: Insert feedback and suggestion into DB.
        instructorService.insertFeedbackForInstructor(instructorId, workshopId, groupNumber, feedback, suggestion);

    }

    // For student review their friends on page review friend
    @RequestMapping(value = "/skills/{student_id}/{workshop_id}/{group_number}", method = RequestMethod.POST)
    void saveFriendSkillScore(@PathVariable("student_id") Long studentId,
            @PathVariable("workshop_id") Long workshopAttId, @PathVariable("group_number") Long groupNumber,
            @RequestParam("skill1") String skill1, @RequestParam("skill2") String skill2,
            @RequestParam("skill3") String skill3, @RequestParam("skill4") String skill4,
            @RequestParam("feedback") String feedback, @RequestParam("suggestion") String suggestion,
            @RequestParam("jsonData") String jsonData, @RequestParam("skIDs") String skIds)
            throws com.fasterxml.jackson.core.JsonParseException, JsonMappingException, IOException {

        System.err.println("SkillController: Student:" + studentId + " workshopAttId:" + workshopAttId + " groupNumber:"
                + groupNumber);
        System.err.println("Skill1 " + skill1);
        System.err.println("Feedback: " + feedback + " Suggestion:" + suggestion);
        System.err.println("JsonData " + jsonData);
        System.err.println("Skill ids " + skIds);

        ArrayList<String> allSkills = new ArrayList<String>();
        allSkills.add(skill1);
        allSkills.add(skill2);
        allSkills.add(skill3);
        allSkills.add(skill4);

        for (int i = 0; i < allSkills.size(); i++) {
            ObjectMapper mapper = new ObjectMapper();
            SkillScore skScore = mapper.readValue(allSkills.get(i), SkillScore.class);
            System.out.println(
                    "************Skill ID: " + skScore.getSkillId() + " Skill Score: " + skScore.getSkillScore());
            userAttWorkshopService.addUserScoreFromReview(studentId, workshopAttId, groupNumber, skScore.getSkillId(),
                    Long.parseLong(skScore.getSkillScore()));
        }

        userFeedbackService.addUserFeedback(studentId, workshopAttId, groupNumber, feedback, suggestion);

    }

}
