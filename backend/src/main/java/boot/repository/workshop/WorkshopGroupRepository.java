package boot.repository.workshop;

import java.util.Collection;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import boot.entity.workshop.WorkshopGroup;

public interface WorkshopGroupRepository extends CrudRepository<WorkshopGroup, Long> {

    String sqlAddGroup = "INSERT INTO workshop_group (workshop_id, group_number) VALUES (?1, ?2);";

    String sqlShowWorkshopGroup = "SELECT * FROM workshop_group " + "WHERE workshop_id = ?1";

    @Transactional
    @Modifying
    @Query(value = sqlAddGroup, nativeQuery = true)
    public int addGroupWorkshop(Long workshopId, Long groupNumber);

    @Query(value = sqlShowWorkshopGroup, nativeQuery = true)
    public Collection<WorkshopGroup> showWorkshopGroup(Long workshopId);
}