package boot.repository.workshop;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.workshop.WorkshopAttToProfile;

public interface WorkshopAttToProfileRepository extends CrudRepository<WorkshopAttToProfile, Long> {
    @Query(value = "SELECT workshop.id, user_att_workshop.user_id, workshop.title, workshop.`start`, workshop.`end` FROM user_att_workshop LEFT JOIN workshop on user_att_workshop.workshop_att_id = workshop.id GROUP BY user_att_workshop.user_id, workshop.title HAVING user_att_workshop.user_id = ?1 ORDER BY workshop.`start`", nativeQuery = true)
    public List<WorkshopAttToProfile> findWorkshopAttToProfileById(long userId);
}