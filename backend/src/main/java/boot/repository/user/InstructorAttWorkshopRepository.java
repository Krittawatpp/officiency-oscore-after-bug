package boot.repository.user;

import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.user.InstructorAttWorkshop;


public interface InstructorAttWorkshopRepository extends CrudRepository<InstructorAttWorkshop, Long> {

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO instructor_score (user_id, workshop_id, skill_id, skill_score) VALUES (?1, ?2, ?3, ?4);", nativeQuery = true)
    public int insertInstructorScore(Long instructorId, Long workshopId, Long skillId, Long skillScore);

}