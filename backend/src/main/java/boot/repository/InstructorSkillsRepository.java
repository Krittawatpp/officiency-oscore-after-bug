package boot.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.user.InstructorSkill;


public interface InstructorSkillsRepository extends CrudRepository<InstructorSkill, Long> {

    @Query(value = "SELECT * FROM instructor_skill", nativeQuery = true)
    public Collection<InstructorSkill> getAllSkills();
}
