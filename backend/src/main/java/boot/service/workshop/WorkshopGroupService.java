package boot.service.workshop;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.workshop.WorkshopGroup;
import boot.repository.workshop.WorkshopGroupRepository;

@Service
public class WorkshopGroupService {
    @Autowired
    private WorkshopGroupRepository workshopGroupRepository;

    public int addWorkshopGroup(Long workshopId, Long groupNumber) {
        return workshopGroupRepository.addGroupWorkshop(workshopId, groupNumber);
    };

    public Collection<WorkshopGroup> showWorkshopGroup(Long workshopId) {
        return workshopGroupRepository.showWorkshopGroup(workshopId);
    }
}