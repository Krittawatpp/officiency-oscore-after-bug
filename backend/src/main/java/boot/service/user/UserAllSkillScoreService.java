package boot.service.user;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.user.UserAllSkillScore;
import boot.repository.user.UserAllSkillScoreRepository;

@Service
public class UserAllSkillScoreService {
    @Autowired
    private UserAllSkillScoreRepository userAllSkillScoreRepository;

    public List<UserAllSkillScore> findUserAllSkillScoreById(Long userId) {
        return userAllSkillScoreRepository.findUserAllSkillScoreByMyId(userId);
    }
}